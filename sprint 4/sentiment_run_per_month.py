import sqlite3
import pandas as pd
import math
from datetime import datetime
import matplotlib.pyplot as plt

import functions

conn = sqlite3.connect("../database/tweets.db")

plt.rcParams["figure.dpi"] = 300  # Set the quality of the images
plt.style.use('tableau-colorblind10')

list_month = ['month_00_doesnt_exist', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
# months to run (don't run 00 or 04 (april))
month_to_run = "07"
month_int = int(month_to_run)
if month_to_run == "":
    print("please enter month in the following format: MM")
elif 12 >= int(month_to_run) >= 0:
    q_compound_AA = f"""
    SELECT "compound", STRFTIME('%w',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned = '1' AND STRFTIME('%m',created_at_datetime) = "{month_to_run}"
    """
    q_compound_BA = f"""
    SELECT "compound", STRFTIME('%w',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned == '1' AND STRFTIME('%m',created_at_datetime) = "{month_to_run}"
    """
    q_compound_rest = f"""
    SELECT "compound", STRFTIME('%w',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned = '0' AND AmericanAir_mentioned = '0' AND STRFTIME('%m',created_at_datetime) = "{month_to_run}"
    """
else:
    assert 1 == 2, Exception("'month_to_run' is not a number (string) between 1 and 12 or an empty string.")

df_AA = pd.read_sql_query(q_compound_AA, conn)
df_BA = pd.read_sql_query(q_compound_BA, conn)
df_rest = pd.read_sql_query(q_compound_rest, conn)
df_AA = df_AA.groupby("week").mean()
df_BA = df_BA.groupby("week").mean()
df_rest = df_rest.groupby("week").mean()
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12, 8))
ax.plot(df_BA)
ax.plot(df_AA)
ax.plot(df_rest)
ax.set_title(f'Mean Sentiment Compound in {list_month[month_int]}', weight='bold', size=24)
ax.set_xlabel('Week', size=20)
ax.set_ylabel('Sentiment compound', size=20)
ax.set_ylim([-0.2, 0.2])
ax.tick_params(labelsize=20)
ax.legend(['British Airways', 'AmericanAir', 'All other airlines'], fontsize="20")
ax.get_figure().savefig("figures/sentiment_run_per_month.png", transparent=True)
print("Successfully saved plot to figures/sentiment_run_per_month.png")
functions.open_image("figures/sentiment_run_per_month.png")

try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")
