import pandas as pd
import sqlite3
import warnings
import matplotlib
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)

import emoji
import re
import time
import gc
from bertopic import BERTopic
from umap import UMAP
import pickle
from sklearn.feature_extraction.text import CountVectorizer
from sentence_transformers import SentenceTransformer
from bertopic.dimensionality import BaseDimensionalityReduction
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA

# define functions
def text_cleaner(text):
    """
    cleans the given text
    :param text: text to clean
    """
    try:
        text = emoji.demojize(text)
    except:
        pass
    text = text.lower()

    # remove all unwanted characters/words
    text = re.sub(r"http\S+|www\S+", "", text)  # links
    text = re.sub(r"@\S+", "", text)  # @'s
    text = re.sub(r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b", "", text)  # mail adresses
    text = re.sub(r"rt ", "", text)  # 'RT '
    text = re.sub(r":\w+:", "", text)  # emoji's
    text = re.sub(r'[_"\-;%()|+&=*%.,!?:#$@\[\]/]', " ", text)  # all (random) characters
    text = re.sub(r" +", " ", text)  # multiple space after each other

    text = text.strip()
    return text


conn = sqlite3.connect("../database/tweets.db")

# set to True if the data has already been cleaned, False otherwise
cleaned_data = True
# set to True if the models have been created already, False otherwise
created_models = True
# set to True to replace the old db table with the new modelled data, False otherwise
create_new_table = True


# clean the data
if not cleaned_data:
    # since topic modelling only works on 1 language per time, I choose to only use English/translated tweets
    q_text = """
    SELECT tweets_en.id_str, translation AS text, tweets.created_at_datetime
    FROM tweets_en, tweets
    WHERE tweets_en.id_str == tweets.id_str AND (tweets.British_Airways_mentioned == '1' OR tweets.AmericanAir_mentioned == '1')
    UNION ALL
    SELECT tweets.id_str, text, created_at_datetime
    FROM tweets
    WHERE id_str NOT IN (SELECT id_str FROM tweets_en) AND lang == 'en' AND (British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1')
    """

    df_text = pd.read_sql_query(q_text, conn)
    # print(df_text)

    t = time.time()
    begin_time = int(t * 1000)

    df_text["clean_text"] = df_text["text"].apply(text_cleaner)

    t = time.time()
    end_time = int(t * 1000)

    # print(df_text)
    print(f"time took: {(end_time-begin_time)/1000:.2f} seconds")

    # remove all empty values
    df_text1 = df_text[df_text["clean_text"] != ""]
    print(f"length of df changed from {len(df_text.index)} to {len(df_text1.index)}, total change is {len(df_text1.index)-len(df_text.index)}")
    df_text1.to_parquet("../database/cleaned_text_for_topics_airlines.parquet")

    del df_text, df_text1
    gc.collect()
else:
    print("Skipped cleaning the data.")


# load the cleaned data
df_cleaned = pd.read_parquet("../database/cleaned_text_for_topics_airlines.parquet")
length_cleaned_data = len(df_cleaned.index)
df0 = df_cleaned.copy()
document = df0["clean_text"].values.tolist()

del df_cleaned
gc.collect()

if not created_models:
    umap_model = UMAP(n_components=2, n_neighbors=15, min_dist=0.0, metric="cosine", low_memory=True)
    # umap_model_empty = BaseDimensionalityReduction()
    # umap_model = UMAP(n_components=5, n_neighbors=15, min_dist=0.0, metric="cosine",)
    sentence_model = SentenceTransformer("all-MiniLM-L6-v2")
    cluster_model = KMeans(n_clusters=301)
    dim_model = PCA(n_components=5)

    # create the model
    vectorizer_model = CountVectorizer(stop_words="english", min_df=20)
    model = BERTopic(nr_topics=301, hdbscan_model=cluster_model, vectorizer_model=vectorizer_model, verbose=True, umap_model=dim_model, embedding_model=sentence_model, calculate_probabilities=False, low_memory=True)
    topics, probs = model.fit_transform(document)

    # save the model
    model.save("models/topic_model0")
    pickle.dump(topics, open("models/topics0.pickle", "wb"))
    print(model.get_topic_freq())
    print(model.get_topic_info()["Name"])
    print(model.get_document_info(document))

    hierarchical_topics = model.hierarchical_topics(document)
    print(hierarchical_topics, hierarchical_topics.columns)

    # since concat and join won't work: we concat it 1 by 1
    # df_model = model.get_document_info(df0["clean_text"]).set_index("Document")
    df0["Document"] = list(model.get_document_info(df0["clean_text"])["Document"])
    df0["Topic"] = list(model.get_document_info(df0["clean_text"])["Topic"])
    df0["Name"] = list(model.get_document_info(df0["clean_text"])["Name"])
    df0["Representation"] = list(model.get_document_info(df0["clean_text"])["Representation"])
    df0["Representative_Docs"] = list(model.get_document_info(df0["clean_text"])["Representative_Docs"])
    df0["Top_n_words"] = list(model.get_document_info(df0["clean_text"])["Top_n_words"])
    # df0["Probability"] = list(model.get_document_info(df0["clean_text"])["Probability"])
    df0["Representative_document"] = list(model.get_document_info(df0["clean_text"])["Representative_document"])

    # save the dataframe
    df0.to_parquet("../database/topic_model.parquet")

    # save the model
    model.save("models/topic_model1")
    pickle.dump(topics, open("models/topics1.pickle", "wb"))
elif created_models:
    print("Loading and applying model...")
    model = BERTopic.load("models/topic_model1")
    # topics, probs = model.transform(document)
    hierarchical_topics = model.hierarchical_topics(document)

# check for equal topics after saving the model
# assert topics == new_topics
# print("assert done")


# visuals
fig = model.visualize_topics()
fig.write_html("figures/topics_scatmap_circles.html")
fig = model.visualize_heatmap()
fig.write_html("figures/topics_heatmap.html")
fig = model.visualize_barchart(n_words=10, height=1000, width=500)
fig.write_html("figures/topics_barchart.html")
fig = model.visualize_hierarchy(hierarchical_topics=hierarchical_topics, height=500, width=1000)
fig.write_html("figures/topics_hierarchy.html")


# save the database
if create_new_table:
    df0 = pd.read_parquet("../database/topic_model.parquet")
    table_name = "topic_model0"
    df0 = df0.reset_index()
    df0 = df0[["id_str", "Document", "Topic", "Name", "Representation", "Representative_Docs", "Top_n_words", "Representative_document"]]
    df0 = df0.astype(str)

    assert len(df0.index) == length_cleaned_data

    # drop old table if there is one
    try:
        q_drop = f"""
        DROP TABLE {table_name}
        """

        conn.execute(q_drop)
    except:
        pass

    # create the new table
    q_create = f"""
    SELECT *
    FROM {table_name}
    """

    num_rows_inserted = df0.to_sql(table_name, conn, index=False)
    cursor = conn.execute(q_create)