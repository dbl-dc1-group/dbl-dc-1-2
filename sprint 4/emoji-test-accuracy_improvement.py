from emot.emo_unicode import UNICODE_EMOJI
from emot.emo_unicode import EMOTICONS_EMO
import sqlite3
import win32com.client
import pandas as pd
import time
import os
import torch
import numpy as np
from transformers import AutoModelForSequenceClassification, AutoTokenizer

conn = sqlite3.connect("../sent_ana_test/database_sent_ana.sqlite")
query_for_emoji_transformation = """
SELECT text, airline_sentiment
FROM Tweets
"""
df_temp = pd.read_sql_query(query_for_emoji_transformation, conn)
df_for_transforming_emojis = df_temp.copy()

emoji_mapping = {**UNICODE_EMOJI, **EMOTICONS_EMO}
print(len(emoji_mapping))

def convert_emojis_and_emoticons(text):
    words = text.split()
    transformed_words = []
    for word in words:
        transformed_word = emoji_mapping.get(word, word)
        if transformed_word == word:
            for emoji in emoji_mapping:
                if emoji in word:
                    transformed_word = emoji_mapping[emoji]
                    break
        transformed_words.append(transformed_word)
    return ' '.join(transformed_words)

t = time.time()
totalbegin_time = int(t * 1000)
calc_length_df = len(df_for_transforming_emojis["text"]) + 1
progressie_to_print = 0.001
for i, tweet in zip(range(calc_length_df), df_for_transforming_emojis["text"]):
    t = time.time()
    begin_time = int(t * 1000)
    df_for_transforming_emojis.loc[i, "text_transformed_emojis"] = convert_emojis_and_emoticons(tweet)
    t = time.time()
    end_time = int(t * 1000)
    elapsed_time = (end_time - begin_time) / 1000
    remaining_time = (len(df_for_transforming_emojis['text_transformed_emojis']) - i) * elapsed_time
    remaining_time = remaining_time / 60 / 60  # Convert to hours
    if i >= int(calc_length_df * progressie_to_print):
        print(f"Progress in percentage: {(i / calc_length_df)*100:.2f}%")
        print(f"Time remaining: {remaining_time:.2f} hours")
        progressie_to_print += 0.001
t = time.time()
totalend_time = int(t * 1000)
accurate_time = ((totalend_time - totalbegin_time) / 1000)
accurate_time_minutes = accurate_time/60
print(f"Time took: {accurate_time:.2f} seconds")
print(f"Time took: {accurate_time_minutes:.2f} minutes")

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model_name = "bertCASE_trained/bert_sentiment_classifier"
tokenizer_name = "bertCASE_trained/bert_sentiment_tokenizer"

tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
model = AutoModelForSequenceClassification.from_pretrained(model_name)
model = model.to(device)

label_mapping = {0: "negative", 1: "neutral", 2: "positive"}

batch_size = 70
t = time.time()
begin_time = int(t * 1000)
progress = 0
percentage_to_print = 0.1

total_batches = len(df_for_transforming_emojis) // batch_size
for i in range(0, len(df_for_transforming_emojis), batch_size):
    progress_useful = (progress / total_batches * 100)
    if progress_useful >= percentage_to_print:
        print(f"Progress till the finish line in percentage: {progress_useful:.2f} %.")
        percentage_to_print += 0.1
    progress += 1
    batch_df = df_for_transforming_emojis.iloc[i:i + batch_size]

    input_texts = batch_df["text"].astype(str).tolist()
    input_tokens = tokenizer.batch_encode_plus(
        input_texts,
        padding=True,
        truncation=True,
        return_tensors="pt"
    )
    input_ids = input_tokens["input_ids"].to(device)

    with torch.no_grad():
        predictions = model(input_ids)[0]

    predicted_labels = torch.argmax(predictions, axis=1).cpu().numpy()
    predicted_sentiments = [label_mapping[label] for label in predicted_labels]
    df_for_transforming_emojis.loc[i:i + batch_size - 1, "Label_analysis_without"] = predicted_sentiments


df_for_transforming_emojis["accuracy_without"] = np.nan
for i in range(0, len(df_for_transforming_emojis)):
    if df_for_transforming_emojis["airline_sentiment"].iloc[i] == df_for_transforming_emojis["Label_analysis_without"].iloc[i]:
        df_for_transforming_emojis.loc[i, "accuracy_without"] = 1
    else:
        df_for_transforming_emojis.loc[i, "accuracy_without"] = 0

total_accuracy = (sum(df_for_transforming_emojis["accuracy_without"])/len(df_for_transforming_emojis["accuracy_without"]))*100
t = time.time()
end_time = int(t * 1000)
accurate_time = ((end_time - begin_time) / 1000)
print(f"Time took: {accurate_time:.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_for_transforming_emojis) * len(df_for_transforming_emojis)):.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_for_transforming_emojis) * len(df_for_transforming_emojis)) / 60:.2f} minutes")
print(f"Time needed: {((accurate_time / len(df_for_transforming_emojis) * len(df_for_transforming_emojis)) / 60) / 60:.2f} hours")
print(f"Total accuracy of distilBERT model: {total_accuracy} %")
batch_size = 70
t = time.time()
begin_time = int(t * 1000)
progress = 0
percentage_to_print = 0.1

total_batches = len(df_for_transforming_emojis) // batch_size
for i in range(0, len(df_for_transforming_emojis), batch_size):
    progress_useful = (progress / total_batches * 100)
    if progress_useful >= percentage_to_print:
        print(f"Progress till the finish line in percentage: {progress_useful:.2f} %.")
        percentage_to_print += 0.1
    progress += 1
    batch_df = df_for_transforming_emojis.iloc[i:i + batch_size]

    input_texts = batch_df["text_transformed_emojis"].astype(str).tolist()
    input_tokens = tokenizer.batch_encode_plus(
        input_texts,
        padding=True,
        truncation=True,
        return_tensors="pt"
    )
    input_ids = input_tokens["input_ids"].to(device)

    with torch.no_grad():
        predictions = model(input_ids)[0]

    predicted_labels = torch.argmax(predictions, axis=1).cpu().numpy()
    predicted_sentiments = [label_mapping[label] for label in predicted_labels]
    df_for_transforming_emojis.loc[i:i + batch_size - 1, "Label_analysis_with"] = predicted_sentiments


df_for_transforming_emojis["accuracy_with"] = np.nan
for i in range(0, len(df_for_transforming_emojis)):
    if df_for_transforming_emojis["airline_sentiment"].iloc[i] == df_for_transforming_emojis["Label_analysis_with"].iloc[i]:
        df_for_transforming_emojis.loc[i, "accuracy_with"] = 1
    else:
        df_for_transforming_emojis.loc[i, "accuracy_with"] = 0

total_accuracy = (sum(df_for_transforming_emojis["accuracy_with"])/len(df_for_transforming_emojis["accuracy_with"]))*100
t = time.time()
end_time = int(t * 1000)
accurate_time = ((end_time - begin_time) / 1000)

try:
    xl = win32com.client.Dispatch("Excel.Application")
    workbooks = xl.Workbooks
    workbook = workbooks("sentiment_results.csv")
    workbook.Close(False)
    xl.Quit()
except:
    print("Failed to close the Excel file.")

df_for_transforming_emojis.to_csv("sentiment_results.csv", index=False)

time.sleep(3)

try:
    excel = win32com.client.Dispatch("Excel.Application")
    excel.Visible = True
    workbook = excel.Workbooks.Open(os.path.abspath("sentiment_results.csv"))
except Exception as e:
    print("Failed to open the file:", str(e))

print(f"Time took: {accurate_time:.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_for_transforming_emojis) * len(df_for_transforming_emojis)):.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_for_transforming_emojis) * len(df_for_transforming_emojis)) / 60:.2f} minutes")
print(f"Time needed: {((accurate_time / len(df_for_transforming_emojis) * len(df_for_transforming_emojis)) / 60) / 60:.2f} hours")
print(f"Total accuracy of distilBERT model: {total_accuracy} %")
