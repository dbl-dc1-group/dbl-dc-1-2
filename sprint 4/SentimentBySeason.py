import sqlite3
import pandas as pd
import math
from datetime import datetime
import matplotlib.pyplot as plt

import functions

conn = sqlite3.connect("../database/tweets.db")

plt.rcParams["figure.dpi"] = 300  # Set the quality of the images
plt.style.use('tableau-colorblind10')

FORCE_GENERATE_NEW_PARQUETS = False

# month 01-12, or empty

winter_week = []
summer_week = []
autumn_week = []

winter = ["52", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
summer = ["26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39"]
autumn = ["40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51"]
for week in winter:
    winter_week.append(str(week))

winter_weeks = ", ".join(winter_week)

for week in summer:
    summer_week.append(str(week))

summer_weeks = ", ".join(summer_week)

for week in autumn:
    autumn_week.append(str(week))

autumn_weeks = ", ".join(autumn_week)

try:
    if FORCE_GENERATE_NEW_PARQUETS:  # Generate an error when FORCE_GENERATE_NEW_PARQUETS is true so that the code run the except code
        crash_list = [1]
        _ = crash_list[2]
    df_compound_BA_winter = pd.read_parquet("../database/df_compound_BA_winter.parquet")
    df_compound_AA_winter = pd.read_parquet("../database/df_compound_AA_winter.parquet")
    df_compound_all_winter = pd.read_parquet("../database/df_compound_all_winter.parquet")

    df_compound_BA_summer = pd.read_parquet("../database/df_compound_BA_summer.parquet")
    df_compound_AA_summer = pd.read_parquet("../database/df_compound_AA_summer.parquet")
    df_compound_all_summer = pd.read_parquet("../database/df_compound_all_summer.parquet")

    df_compound_BA_autumn = pd.read_parquet("../database/df_compound_BA_autumn.parquet")
    df_compound_AA_autumn = pd.read_parquet("../database/df_compound_AA_autumn.parquet")
    df_compound_all_autumn = pd.read_parquet("../database/df_compound_all_autumn.parquet")

    print("Parquet files exists, skipping SQL queries to save time!")

except:  # Cannot load the parquets
    if FORCE_GENERATE_NEW_PARQUETS:
        print("You forced to generate new parquets, running SQL queries...")
    else:
        print("No parquets found, running SQL queries...")
    # British airways
    q_compound_BA_winter = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned == '1' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 356 OR CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 79)
        """
    df_compound_BA_winter = pd.read_sql_query(q_compound_BA_winter, conn)
    df_compound_BA_winter = df_compound_BA_winter.groupby("week").mean()
    # AmericanAir
    q_compound_AA_winter = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 356 OR CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 79)
        """
    df_compound_AA_winter = pd.read_sql_query(q_compound_AA_winter, conn)
    df_compound_AA_winter = df_compound_AA_winter.groupby("week").mean()
    # all airways
    q_compound_all_winter = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '0' AND British_Airways_mentioned == '0' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 356 OR CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 79)
         """
    df_compound_all_winter = pd.read_sql_query(q_compound_all_winter, conn)
    df_compound_all_winter = df_compound_all_winter.groupby("week").mean()

    print("Winter queries done! (1/3)")

    # summer
    q_compound_BA_summer = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned == '1' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 173 AND CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 266)
          """
    df_compound_BA_summer = pd.read_sql_query(q_compound_BA_summer, conn)
    df_compound_BA_summer = df_compound_BA_summer.groupby("week").mean()
    # AmericanAir
    q_compound_AA_summer = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 173 AND CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 266)
        """
    df_compound_AA_summer = pd.read_sql_query(q_compound_AA_summer, conn)
    df_compound_AA_summer = df_compound_AA_summer.groupby("week").mean()
    # all airways
    q_compound_all_summer = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '0' AND British_Airways_mentioned == '0' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 173 AND CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 266)
        """
    df_compound_all_summer = pd.read_sql_query(q_compound_all_summer, conn)
    df_compound_all_summer = df_compound_all_summer.groupby("week").mean()

    print("Summer queries done! (2/3)")

    q_compound_BA_autumn = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned == '1' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 266 AND CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 356)
        """
    df_compound_BA_autumn = pd.read_sql_query(q_compound_BA_autumn, conn)
    df_compound_BA_autumn = df_compound_BA_autumn.groupby("week").mean()
    # AmericanAir
    q_compound_AA_autumn = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 266 AND CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 356)
        """
    df_compound_AA_autumn = pd.read_sql_query(q_compound_AA_autumn, conn)
    df_compound_AA_autumn = df_compound_AA_autumn.groupby("week").mean()
    # all airways
    q_compound_all_autumn = f"""
        SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
        FROM tweets, sentiment_BERT
        WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '0' AND British_Airways_mentioned == '0' AND (CAST(STRFTIME('%j', created_at_datetime) AS INTEGER)  > 266 AND CAST(STRFTIME('%j', created_at_datetime) AS INTEGER) < 356)
        """
    df_compound_all_autumn = pd.read_sql_query(q_compound_all_autumn, conn)
    df_compound_all_autumn = df_compound_all_autumn.groupby("week").mean()

    print("Autumn queries done! (3/3)")
    print("Saving dataframes to parquets...")

    # Save dataframes
    df_compound_BA_winter.to_parquet("../database/df_compound_BA_winter.parquet")
    df_compound_AA_winter.to_parquet("../database/df_compound_AA_winter.parquet")
    df_compound_all_winter.to_parquet("../database/df_compound_all_winter.parquet")

    df_compound_BA_summer.to_parquet("../database/df_compound_BA_summer.parquet")
    df_compound_AA_summer.to_parquet("../database/df_compound_AA_summer.parquet")
    df_compound_all_summer.to_parquet("../database/df_compound_all_summer.parquet")

    df_compound_BA_autumn.to_parquet("../database/df_compound_BA_autumn.parquet")
    df_compound_AA_autumn.to_parquet("../database/df_compound_AA_autumn.parquet")
    df_compound_all_autumn.to_parquet("../database/df_compound_all_autumn.parquet")

    print("Successfully save dataframes to parquets to save time next time!")


week_labels = ['52', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']

mean_summer_overall = (df_compound_BA_summer["compound"].mean()+df_compound_AA_summer["compound"].mean()+df_compound_all_summer["compound"].mean())/3
print(f"Summer mean: {mean_summer_overall}")

mean_winter_overall = (df_compound_BA_winter["compound"].mean()+df_compound_AA_winter["compound"].mean()+df_compound_all_winter["compound"].mean())/3
print(f"Winter mean: {mean_winter_overall}")

mean_autumn_overall = (df_compound_BA_autumn["compound"].mean()+df_compound_AA_autumn["compound"].mean()+df_compound_all_autumn["compound"].mean())/3
print(f"Autumn mean: {mean_autumn_overall}")

print("Plotting winter...")
# Plot 1 (Winter)
fig, ax = plt.subplots(figsize=(12, 14), nrows=3)
ax[0].plot(df_compound_BA_winter)
ax[0].plot(df_compound_AA_winter)
ax[0].plot(df_compound_all_winter)
ax[0].set_title('Mean Sentiment Compound for the Winter Months', weight='bold', size=24)
ax[0].set_xlabel('Weeks', size=20)
ax[0].set_ylabel('Sentiment compound', size=20)
ax[0].set_xlim([0, 12])
ax[0].set_ylim([-0.2, 0.2])
ax[0].set_xticks(range(len(week_labels)))
ax[0].set_xticklabels(week_labels)
ax[0].tick_params(labelsize=20)
ax[0].legend(['British Airways', 'AmericanAir', 'All other airlines'], loc='upper right', fontsize="18")

# plt.tight_layout()  # Adjust subplot spacing
#
# plt.savefig("figures/topics/sentiment_winter.png", transparent=True)
# plt.show()

# Plot 2 (Summer)
# fig, ax = plt.subplots(figsize=(12, 8))
print("Plotting summer...")
ax[1].plot(df_compound_BA_summer)
ax[1].plot(df_compound_AA_summer)
ax[1].plot(df_compound_all_summer)
ax[1].set_title('Mean Sentiment Compound for the Summer Months', weight='bold', size=24)
ax[1].set_xlabel('Weeks', size=20)
ax[1].set_ylabel('Sentiment compound', size=20)
ax[1].set_ylim([-0.2, 0.2])
ax[1].tick_params(labelsize=20)
ax[1].legend(['British Airways', 'AmericanAir', 'All other airlines'], loc='upper right', fontsize="18")

# plt.tight_layout()  # Adjust subplot spacing
#
# plt.savefig("figures/topics/sentiment_summer.png", transparent=True)
# plt.show()

# Plot 3 (Autumn)
# fig, ax = plt.subplots(figsize=(12, 8))
print("Plotting autumn...")
ax[2].plot(df_compound_BA_autumn)
ax[2].plot(df_compound_AA_autumn)
ax[2].plot(df_compound_all_autumn)
ax[2].set_title('Mean Sentiment Compound for the Autumn Months', weight='bold', size=24)
ax[2].set_xlabel('Weeks', size=20)
ax[2].set_ylabel('Sentiment compound', size=20)
ax[2].tick_params(labelsize=20)
ax[2].set_ylim([-0.2, 0.2])
ax[2].legend(['British Airways', 'AmericanAir', 'All other airlines'], loc='upper right', fontsize="18")

plt.tight_layout(h_pad=.5)  # Adjust subplot spacing

# plt.savefig("figures/topics/sentiment_autumn.png", transparent=True)
fig.get_figure().savefig("figures/topics/sentiment_per_season.png", transparent=True)
print("Successfully saved the plot to figures/topics/sentiment_per_season.png")
functions.open_image("figures/topics/sentiment_per_season.png")
try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")
