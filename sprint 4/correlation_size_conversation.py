import gc
import pandas as pd
import sqlite3
import time
import pickle
import functions
import variables
import numpy as np
import matplotlib.pyplot as plt

MONTH = ""

plt.rcParams["figure.dpi"] = 300

conn = sqlite3.connect("../database/tweets.db")
month_dct = {"04": "April (undefined Year)", "05": "May 2019", "06": "June 2019", "07": "July 2019",
             "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020", "": "May 2019 - March 2020"}
with open("../convs/sentiment.pkl", "rb") as sentiment_pickle:
    sentiment = pickle.load(sentiment_pickle)
    sentiment_items = list(sentiment.items())[:10]  # Get the first 10 items

with open("../convs/users.pkl", "rb") as users_pickle:
    users = pickle.load(users_pickle)
    users_items = list(users.items())[:10]  # Get the first 10 items

with open("../convs/conversations.pkl", "rb") as conversations_pickle:
    conversations = pickle.load(conversations_pickle)
    conversations_items = list(conversations.items())[:10]  # Get the first 10 items


def get_sentiment_change(replies):
    replies = eval(replies)
    try:
        if users[replies[0]] == users[replies[-1]]:
            sentiment_change = sentiment[replies[-1]] - sentiment[replies[0]]
            return sentiment_change
        elif users[replies[0]] == users[replies[-2]]:
            sentiment_change = sentiment[replies[-2]] - sentiment[replies[0]]
            return sentiment_change
        else:
            return None
    except:
        return None


length_of_conversation = {}

if MONTH == "":
    query_length = """
    SELECT length_convo, replies
    FROM convos
    """
else:
    query_length = f"""
    SELECT length_convo, replies
    FROM convos
    WHERE convos.top_tweet IN (SELECT id_str FROM tweets WHERE STRFTIME("%m", created_at_datetime) == '{MONTH}')
    """

df_length = pd.read_sql_query(query_length, conn)
print(f'Done extracting data from database')
df_length.sort_values('length_convo', inplace=True)
df_length['sentiment_change'] = df_length['replies'].apply(get_sentiment_change)
df_length = df_length[~df_length['sentiment_change'].isna()]
df_length['sentiment_mean'] = df_length.groupby('length_convo')['sentiment_change'].transform('mean')

# Create a figure and axis
fig, ax = plt.subplots(figsize=(12, 8))

# Scatter plot
ax.scatter(df_length['length_convo'], df_length['sentiment_mean'], label='Data Points')

# Regression line
regression_line = np.polyfit(df_length['length_convo'], df_length['sentiment_change'], deg=3)
regression_values = np.polyval(regression_line, df_length['length_convo'])
ax.plot(df_length['length_convo'], regression_values, color='red', label='Regression Line')

# Set labels and title
ax.set_xlabel('Conversation Length', size=20)
ax.set_ylabel('Sentiment Mean', size=20)
ax.tick_params(labelsize=20)
ax.set_title(f'Sentiment Mean vs Conversation Length\nin {month_dct[MONTH]}', weight='bold', size=24)

# Display legend
ax.legend(fontsize="20")
plt.tight_layout()
ax.get_figure().savefig(f'figures/corr_sent_convo_{month_dct[MONTH]}.png', transparent=True)
print(f"Saved plot to figures/corr_sent_convo_{month_dct[MONTH]}.png")
functions.open_image(f"figures/corr_sent_convo_{month_dct[MONTH]}.png")
# Show the combined plot
try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")

# Calculate R-squared value
residuals = df_length['sentiment_mean'] - regression_values
mean_y = np.mean(df_length['sentiment_mean'])
ss_total = np.sum((df_length['sentiment_mean'] - mean_y) ** 2)
ss_residual = np.sum(residuals ** 2)
r_squared = 1 - (ss_residual / ss_total)

print("R-squared value:", r_squared)

# Calculate correlation
correlation = df_length['length_convo'].corr(df_length['sentiment_mean'])
print("Correlation between sentiment mean and conversation length:", correlation)
