import time
import pandas as pd
import numpy as np
import sqlite3
import gc
import os
import subprocess
import win32com.client
import torch
from transformers import AutoModelForSequenceClassification, AutoTokenizer

# if sentiment analysis has already been put into parquet files, set to False. True otherwise
do_analysis = True


def calculate_time_remaining(progress, total_batches, elapsed_time):
    if progress == 0:
        return "Calculating..."
    average_batch_time = elapsed_time / progress
    remaining_batches = total_batches - progress
    estimated_remaining_time = remaining_batches * average_batch_time
    estimated_remaining_hours = estimated_remaining_time / 3600
    return f"Estimated time remaining: {estimated_remaining_hours:.2f} hours"


conn = sqlite3.connect("../database/tweets.db")
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model_name = "bertCASE_trained/bert_sentiment_classifier"
tokenizer_name = "bertCASE_trained/bert_sentiment_tokenizer"

tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
model = AutoModelForSequenceClassification.from_pretrained(model_name)
model = model.to(device)
label_mapping2 = {0: "negative", 1: "neutral", 2: "positive"}

if do_analysis:
    # batch_size = 70
    # t = time.time()
    # begin_time = int(t * 1000)
    # progress = 0
    # percentage_to_print = 0.1
    # time_interval = 1
    # q_tweets = """
    # SELECT id_str, text_WITHOUTEMOJI, lang
    # FROM tweets
    # WHERE lang == "en" OR lang == "und"
    # """
    # df_tweets = pd.read_sql_query(q_tweets, conn)
    # df_tweets["sentiment_BERT"] = ""
    # df_tweets["values_BERT"] = ""
    # total_batches = len(df_tweets) // batch_size
    #
    # formatted_values_batch = []
    #
    # for i in range(0, len(df_tweets), batch_size):
    #     progress_useful = (progress / total_batches * 100)
    #     if progress_useful >= time_interval:
    #         t = time.time()
    #         current_time = int(t * 1000)
    #         elapsed_time = (current_time - begin_time) / 1000
    #
    #         time_remaining = calculate_time_remaining(progress, total_batches, elapsed_time)
    #         print(f"Progress till the finish line in percentage: {progress_useful:.2f} % and {time_remaining}.")
    #         percentage_to_print += 0.1
    #         time_interval += 1
    #
    #     elif progress_useful >= percentage_to_print:
    #         print(f"Progress till the finish line in percentage: {progress_useful:.2f} %.")
    #         percentage_to_print += 0.1
    #
    #     progress += 1
    #     batch_df = df_tweets.iloc[i:i + batch_size]
    #
    #     input_texts = batch_df["text_WITHOUTEMOJI"].astype(str).tolist()
    #     input_tokens = tokenizer.batch_encode_plus(
    #         input_texts,
    #         padding=True,
    #         truncation=True,
    #         return_tensors="pt"
    #     )
    #     input_ids = input_tokens["input_ids"].to(device)
    #
    #     with torch.no_grad():
    #         predictions = model(input_ids)[0]
    #         predictions_cpu = predictions.cpu()
    #         x = predictions_cpu.tolist()
    #     predicted_labels = torch.argmax(predictions, axis=1).cpu().numpy()
    #     predicted_sentiments = [label_mapping2[label] for label in predicted_labels]
    #     df_tweets.loc[i:i + batch_size - 1, "sentiment_BERT"] = predicted_sentiments
    #     values_to_assign = x[:min(batch_size, len(df_tweets) - i)]
    #     for j, value in enumerate(values_to_assign):
    #         rounded_value = [round(v, 2) for v in value]
    #         df_tweets.at[i + j, "values_BERT"] = rounded_value
    #
    # df_tweets[["id_str", "sentiment_BERT", "values_BERT", "lang"]].to_parquet("../database/tweets_ana.parquet_BERT")
    # print("parquet file created")
    # t = time.time()
    # end_time = int(t * 1000)
    # accurate_time = ((end_time - begin_time) / 1000)
    # print(f"Time took: {accurate_time:.2f} seconds")
    # print(f"Time needed: {(accurate_time / len(df_tweets) * len(df_tweets)):.2f} seconds")
    # print(f"Time needed: {(accurate_time / len(df_tweets) * len(df_tweets)) / 60:.2f} minutes")
    # print(f"Time needed: {((accurate_time / len(df_tweets) * len(df_tweets)) / 60) / 60:.2f} hours")
    # print(f"Total size of set: {len(df_tweets)}")
    # del df_tweets
    # gc.collect()

    batch_size = 70
    time_interval_en = 1
    t = time.time()
    begin_time = int(t * 1000)
    progress = 0
    percentage_to_print = 0.1
    q_tweets_en = """
    SELECT tweets.id_str, tweets_en.text_WITHOUTEMOJI AS text, lang
    FROM tweets, tweets_en
    WHERE tweets.id_str == tweets_en.id_str
    """
    df_tweets_en = pd.read_sql_query(q_tweets_en, conn)
    df_tweets_en["sentiment_BERT"] = ""
    df_tweets_en["values_BERT"] = ""
    total_batches_en = len(df_tweets_en) // batch_size
    for i in range(0, len(df_tweets_en), batch_size):
        progress_useful = (progress / total_batches_en * 100)
        if progress_useful >= time_interval_en:
            t = time.time()
            current_time = int(t * 1000)
            elapsed_time = (current_time - begin_time) / 1000

            time_remaining = calculate_time_remaining(progress, total_batches_en, elapsed_time)
            print(f"Progress till the finish line in percentage: {progress_useful:.2f} % and {time_remaining}.")
            percentage_to_print += 0.1
            time_interval_en += 1

        elif progress_useful >= percentage_to_print:
            print(f"Progress till the finish line in percentage: {progress_useful:.2f} %.")
            percentage_to_print += 0.1

        progress += 1
        batch_df = df_tweets_en.iloc[i:i + batch_size]

        input_texts = batch_df["text"].astype(str).tolist()
        input_tokens = tokenizer.batch_encode_plus(
            input_texts,
            padding=True,
            truncation=True,
            return_tensors="pt"
        )
        input_ids = input_tokens["input_ids"].to(device)

        with torch.no_grad():
            predictions = model(input_ids)[0]
            predictions_cpu = predictions.cpu()
            x = predictions_cpu.tolist()
        predicted_labels = torch.argmax(predictions, axis=1).cpu().numpy()
        predicted_sentiments = [label_mapping2[label] for label in predicted_labels]
        df_tweets_en.loc[i:i + batch_size - 1, "sentiment_BERT"] = predicted_sentiments
        values_to_assign = x[:min(batch_size, len(df_tweets_en) - i)]
        for j, value in enumerate(values_to_assign):
            rounded_value = [round(v, 2) for v in value]
            df_tweets_en.at[i + j, "values_BERT"] = rounded_value

    df_tweets_en[["id_str", "sentiment_BERT", "values_BERT", "lang"]].to_parquet(
        "../database/tweets_en_ana.parquet_BERT")
    print("parquet file created")
    t = time.time()
    end_time = int(t * 1000)
    accurate_time = ((end_time - begin_time) / 1000)
    print(f"Time took: {accurate_time:.2f} seconds")
    print(f"Time needed: {(accurate_time / len(df_tweets_en) * len(df_tweets_en)):.2f} seconds")
    print(f"Time needed: {(accurate_time / len(df_tweets_en) * len(df_tweets_en)) / 60:.2f} minutes")
    print(f"Time needed: {((accurate_time / len(df_tweets_en) * len(df_tweets_en)) / 60) / 60:.2f} hours")
    print(f"Total size of set: {len(df_tweets_en)}")

    del df_tweets_en
    gc.collect()

df_tweets = pd.read_parquet("../database/tweets_ana.parquet_BERT")
df_tweets_en = pd.read_parquet("../database/tweets_en_ana.parquet_BERT")

sentiment_tweets = []
df_tweets_length = len(df_tweets.index)
df_tweets_en_length = len(df_tweets_en.index)

df_sentiment_tweets = pd.concat([df_tweets, df_tweets_en]).astype(str)
sentiments_tweets = []
count = 0
number_of_tweets = len(df_sentiment_tweets.index)
t = time.time()
begin_time = int(t * 1000)
for row in df_sentiment_tweets.iterrows():
    id = row[1]["id_str"]
    sentiment = row[1]["sentiment_BERT"]
    values = row[1]["values_BERT"]
    lang = row[1]["lang"]
    sentiments_tweets.append({"id": id, "sentiment": sentiment, "values": values, "lang": lang})
    count += 1
    if count % 100000 == 0:
        t = time.time()
        end_time = int(t * 1000)
        time_took = (end_time - begin_time) / 1000
        print(
            f"100.000 iterations of setting up the database took {time_took:.2f} seconds (at {(count / number_of_tweets * 100):.2f}% and still {((number_of_tweets - count) / 100000 * time_took):.2f} seconds to go)")
        begin_time = end_time

df_sentiment_tweets = pd.DataFrame(sentiments_tweets)
df_sentiment_tweets.to_parquet("../database/sentiment_tweets_BERT.parquet")

table_name = "sentiment_BERT"
try:
    q = f"""
    DROP TABLE {table_name}
    """
    conn.execute(q)
except:
    pass

query = f"""
SELECT *
FROM {table_name}
"""

num_rows_inserted = df_sentiment_tweets.to_sql(table_name, conn, index=False)
cursor = conn.execute(query)
print('Sentiments successfully saved to tweets.db!')
