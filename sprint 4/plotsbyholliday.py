import sqlite3
import pandas as pd
import math
from datetime import datetime
import matplotlib.pyplot as plt

conn = sqlite3.connect("../database/tweets.db")

plt.rcParams["figure.dpi"] = 300  # Set the quality of the images

# month 01-12, or empty
month = ""

# British Airways
q_compound_BA = f"""
SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
FROM tweets, sentiment_BERT
WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND (STRFTIME("%m", created_at_datetime) == '07' OR STRFTIME("%m", created_at_datetime) == '08')
    """

df_weeklycompound_BA = pd.read_sql_query(q_compound_BA, conn)
df_weeklycompound_BA = df_weeklycompound_BA.groupby("week").mean()

ax_weekly_sentiment_BA = df_weeklycompound_BA.plot(kind="line")
ax_weekly_sentiment_BA.set_ylabel("compound")
ax_weekly_sentiment_BA.set_xlabel("week of the year")
ax_weekly_sentiment_BA.set_title("average weekly sentiment British airways")
plt.ylim(-0.15,0)
plt.show()
ax_weekly_sentiment_BA.get_figure().savefig("figures/weeklysentiment_britishairways.png", transparent=True)
#AmericanAir
q_compound_AA = f"""
SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
FROM tweets, sentiment_BERT
WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND (STRFTIME("%m", created_at_datetime) == '06' OR STRFTIME("%m", created_at_datetime) == '07' OR STRFTIME("%m", created_at_datetime) == '08')
    """


df_weeklycompound_AA = pd.read_sql_query(q_compound_AA, conn)
print(df_weeklycompound_AA)
df_weeklycompound_AA = df_weeklycompound_AA.groupby("week").mean()

ax_weekly_sentiment_AA = df_weeklycompound_AA.plot(kind="line")
ax_weekly_sentiment_AA.set_ylabel("compound")
ax_weekly_sentiment_AA.set_xlabel("week of the year")
ax_weekly_sentiment_AA.set_title("average weekly sentiment AmericanAir")
plt.ylim(-0.15,0)
plt.show()
ax_weekly_sentiment_AA.get_figure().savefig("figures/weeklysentiment_Americanair.png", transparent=True)
#mean of all

q_compound = f"""
SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
FROM tweets, sentiment_BERT
WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND (STRFTIME("%m", created_at_datetime) == '05' OR STRFTIME("%m", created_at_datetime) == '06' OR STRFTIME("%m", created_at_datetime) == '07')
    """
df_weeklycompound = pd.read_sql_query(q_compound, conn)
df_weeklycompound = df_weeklycompound.groupby("week").mean()

ax_weekly_sentiment = df_weeklycompound.plot(kind="line")
ax_weekly_sentiment.set_ylabel("compound")
ax_weekly_sentiment.set_xlabel("week of the year")
ax_weekly_sentiment.set_title("average weekly sentiment all airways")
plt.ylim(-0.15,0)
plt.show()
ax_weekly_sentiment.get_figure().savefig("figures/weeklysentiment_all_airways.png", transparent=True)


#for