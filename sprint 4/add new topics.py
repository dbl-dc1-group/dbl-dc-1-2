import sqlite3
import pandas as pd

conn = sqlite3.connect("../database/tweets.db")

# British Airways and main competitor
table_name = "topic_model0"

# NOTE: you can only run this ones, after you have runned both sentiment topic files.
try:
    try:
        df_new_topics = pd.read_excel("../Topics_merge.xlsx")
        df_new_topics = df_new_topics.astype(str)
        df_new_topics = df_new_topics[df_new_topics["of_importance"] == "1"]

        try:
            q_drop0 = """
            DROP TABLE "Topics_merge"
            """
            conn.execute(q_drop0)
        except Exception as e:
            print(e)

        q_create0 = """
        SELECT *
        FROM "Topics_merge"
        """
        num_rows_inserted = df_new_topics.to_sql("Topics_merge", conn, index=False)
        cursor = conn.execute(q_create0)
    except Exception as e:
        print(e)

    cursor.close()

    # extract all new topic names
    q_n_topics = """
    SELECT id_str, Document, topic_model0.Topic, topic_model0.Name, Representation, Representative_Docs, topic_model0.Top_n_words, Representative_document, Related_to AS New_Topic
    FROM topic_model0, Topics_merge
    WHERE topic_model0.Topic == Topics_merge.Topic
    """
    q_n_topics1 = """
    SELECT *, Name AS New_Topic
    FROM topic_model0
    WHERE Topic NOT IN (SELECT Topic FROM Topics_merge)
    """
    df_n_topics = pd.read_sql_query(q_n_topics, conn)
    df_n_topics1 = pd.read_sql_query(q_n_topics1, conn)
    df_n_topics["of_importance"] = "1"
    df_n_topics1["of_importance"] = "0"

    df_final0 = pd.concat([df_n_topics, df_n_topics1], ignore_index=True)

    try:
        q_drop0 = """
        DROP TABLE "Topics_merge"
        """
        conn.execute(q_drop0)

        q_drop = f"""
        DROP TABLE {table_name}
        """
        conn.execute(q_drop)
    except Exception as e:
        print(e)

    q_create = f"""
    SELECT *
    FROM {table_name}
    """

    num_rows_inserted = df_final0.to_sql(table_name, conn, index=False)
    cursor = conn.execute(q_create)
    cursor.close()
except Exception as e:
    print(e)
    print("\033[93m Model with main airlines failed to convert, try to run 'sentiment topics.py' first before running this again.")


# ---------------------------------all other competitors----------------------------------------------------------------
table_name = "topic_competitors_model0"

try:
    try:
        df_new_topics = pd.read_excel("../Topics_merge_comp.xlsx")
        df_new_topics = df_new_topics.astype(str)
        df_new_topics = df_new_topics[df_new_topics["of_importance"] == "1"]

        try:
            q_drop0 = """
            DROP TABLE "Topics_merge_comp"
            """
            conn.execute(q_drop0)
        except Exception as e:
            print(e)

        q_create0 = """
        SELECT *
        FROM "Topics_merge_comp"
        """
        num_rows_inserted = df_new_topics.to_sql("Topics_merge_comp", conn, index=False)
        cursor = conn.execute(q_create0)
    except Exception as e:
        print(e)

    cursor.close()

    # extract all new topic names
    q_n_topics = """
    SELECT id_str, Document, topic_competitors_model0.Topic, topic_competitors_model0.Name, Representation, Representative_Docs, topic_competitors_model0.Top_n_words, Representative_document, Related_to AS New_Topic, of_importance
    FROM topic_competitors_model0, Topics_merge_comp
    WHERE topic_competitors_model0.Topic == Topics_merge_comp.Topic
    """

    q_n_topics1 = """
    SELECT *, Name AS New_Topic
    FROM topic_competitors_model0
    WHERE Topic NOT IN (SELECT Topic FROM Topics_merge_comp)
    """
    df_n_topics = pd.read_sql_query(q_n_topics, conn)
    df_n_topics1 = pd.read_sql_query(q_n_topics1, conn)
    df_n_topics["of_importance"] = "1"
    df_n_topics1["of_importance"] = "0"

    df_final1 = pd.concat([df_n_topics, df_n_topics1], ignore_index=True)

    try:
        q_drop0 = """
        DROP TABLE "Topics_merge_comp"
        """
        conn.execute(q_drop0)

        q_drop = f"""
        DROP TABLE {table_name}
        """
        conn.execute(q_drop)
    except Exception as e:
        print(e)

    q_create = f"""
    SELECT *
    FROM {table_name}
    """

    num_rows_inserted = df_final1.to_sql(table_name, conn, index=False)
    cursor = conn.execute(q_create)
except Exception as e:
    print(e)
    print("\033[93m Model with other competitors failed to convert, try to run 'sentiment topics other_competitors.py' first before running this again.")

