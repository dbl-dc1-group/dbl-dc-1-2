import os

import matplotlib.pyplot as plt
import pandas as pd
import sqlite3

import functions
import variables

conn = sqlite3.connect("../database/tweets.db")

plt.rcParams["figure.dpi"] = 300  # Set the quality of the images

month_dct = {"04": "April (undefined Year)", "05": "May 2019", "06": "June 2019", "07": "July 2019", "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020", "": "May 2019 - March 2020"}


MONTH = ""
select_list = []
for airline in variables.AIRLINES.keys():
    select_list.append(f"{airline}_mentioned AS {airline}")

select_str = ", ".join(select_list)

print("Running SQL queries")
if MONTH == "":
    query_sent_airlines = f"""
    SELECT user_id_str
    FROM tweets
    WHERE user_id_str IN {tuple(list(variables.AIRLINES.values()))}
    """

    query_received_airlines = f"""
    SELECT {select_str}
    FROM tweets
    """
else:
    query_sent_airlines = f"""
    SELECT user_id_str
    FROM tweets
    WHERE STRFTIME("%m", created_at_datetime) == '{MONTH}' AND user_id_str IN {tuple(list(variables.AIRLINES.values()))}
    """

    query_received_airlines = f"""
    SELECT {select_str}
    FROM tweets
    WHERE STRFTIME("%m", created_at_datetime) == '{MONTH}'
    """

df_sent = pd.read_sql_query(query_sent_airlines, conn)

df_received = pd.read_sql_query(query_received_airlines, conn)

print("Successfully ran SQL queries!")
print("Plotting data...")

sent_vs_mentioned_dct = []
for airline in variables.AIRLINES.keys():
    if airline != "AirBerlin" and airline != "AirBerlin_assist":  # No data for these airline so we can leave them out
        number_mentioned = len(df_received[df_received[airline] == "1"].index)
        number_sent = len(df_sent[df_sent["user_id_str"] == str(variables.AIRLINES[airline])].index)
        print(f"{airline} has sent {number_sent} tweets and received {number_mentioned} tweets ({(number_sent/number_mentioned)*100}%)")
        sent_vs_mentioned_dct.append({"airline": airline, "percentage_sent_mentioned": (number_sent/number_mentioned)*100})


df_sent_vs_mentioned = pd.DataFrame(sent_vs_mentioned_dct)

# Sort data
df_sent_vs_mentioned = df_sent_vs_mentioned.sort_values("percentage_sent_mentioned", ascending=False)

# Plot data
plot_vs = df_sent_vs_mentioned.plot(kind="bar", x="airline", y="percentage_sent_mentioned", figsize=(12, 8))
plot_vs.set_title(f"Answered percentage per airline\nin {month_dct[MONTH]}", size=24, weight="bold")
plot_vs.set_xlabel("Airline", size=20)
plot_vs.set_ylabel("Answered percentage (%)", size=20)
plot_vs.tick_params(labelsize=20)
plot_vs.legend().remove()
plt.xticks(rotation=45)
plt.tight_layout()

plot_vs.get_figure().savefig("figures/sent_vs_mentioned.png", transparent=True)
print(df_sent_vs_mentioned)
print("Plot successfully saved to figures/sent_vs_mentioned.png")
try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")
functions.open_image("figures/sent_vs_mentioned.png")
