import pickle
import emot
import pandas as pd
from emot.emo_unicode import UNICODE_EMOJI
from emot.emo_unicode import EMOTICONS_EMO
import sqlite3
import time

df_read = pd.read_parquet("../database/tweets_WITHOUT_EMOJI_tweets.parquet")

table_name = "tweets"
conn = sqlite3.connect("../database/tweets.db")
existing_table_df = pd.read_sql_query('SELECT * FROM tweets', conn)

existing_table_df["text_WITHOUTEMOJI"] = df_read["text_transformed_emojis"]

try:
    q = f"DROP TABLE IF EXISTS {table_name}"
    conn.execute(q)
except Exception as e:
    print(f"Error dropping table: {e}")

num_rows_inserted = existing_table_df.to_sql(table_name, conn, if_exists='replace', index=False)

print('Tweets successfully saved to tweets.db!')

df_read = pd.read_parquet("../database/tweets_WITHOUT_EMOJI_tweets_en.parquet")

table_name = "tweets_en"
conn = sqlite3.connect("../database/tweets.db")
existing_table_df = pd.read_sql_query('SELECT * FROM tweets_en', conn)

existing_table_df["text_WITHOUTEMOJI"] = df_read["text_transformed_emojis"]

try:
    q = f"DROP TABLE IF EXISTS {table_name}"
    conn.execute(q)
except Exception as e:
    print(f"Error dropping table: {e}")

num_rows_inserted = existing_table_df.to_sql(table_name, conn, if_exists='replace', index=False)

print('Tweets successfully saved to tweets.db!')
