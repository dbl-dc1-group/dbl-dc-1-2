import pandas as pd
import time
import sqlite3
from transformers import AutoModelForSequenceClassification, AutoTokenizer
import torch
from torch.utils.data import DataLoader, Dataset
from sklearn.model_selection import train_test_split

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model_name = "../sprint 3/models/distilbert_sentiment_classifier"
tokenizer_name = "../sprint 3/models/distilbert_sentiment_tokenizer"
tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
model = AutoModelForSequenceClassification.from_pretrained(model_name, num_labels=3, from_tf=True)
model.to(device)

df_tweets_at_ba_2 = pd.read_csv("../sent_ana_test/train2.0.csv", encoding='latin-1')
df_tweets_at_ba_2_copy = df_tweets_at_ba_2.copy(deep=True)

label_mapping = {
    "negative": 0,
    "neutral": 1,
    "positive": 2
}

df_tweets_at_ba_2_copy["label"] = df_tweets_at_ba_2_copy["sentiment"].map(label_mapping)
train_df, val_df = train_test_split(df_tweets_at_ba_2_copy, test_size=0.2, random_state=42)


class SentimentDataset(Dataset):
    def __init__(self, tokenizer, df):
        self.encodings = tokenizer(df["text"].astype(str).tolist(), truncation=True, padding=True)
        self.labels = df["label"].tolist()

    def __getitem__(self, idx):
        item = {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}
        item["labels"] = torch.tensor(self.labels[idx])
        return item

    def __len__(self):
        return len(self.labels)


train_dataset = SentimentDataset(tokenizer, train_df)
val_dataset = SentimentDataset(tokenizer, val_df)

train_loader = DataLoader(train_dataset, batch_size=16, shuffle=True)
val_loader = DataLoader(val_dataset, batch_size=16)

optimizer = torch.optim.AdamW(model.parameters(), lr=5e-5)
loss_fn = torch.nn.CrossEntropyLoss()

num_epochs = 3
model.train()
for epoch in range(num_epochs):
    total_loss = 0
    total_correct = 0
    for batch in train_loader:
        input_ids = batch["input_ids"].to(device)
        attention_mask = batch["attention_mask"].to(device)
        labels = batch["labels"].to(device)

        optimizer.zero_grad()
        outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
        logits = outputs.logits
        loss = loss_fn(logits, labels)

        total_loss += loss.item()
        total_correct += (logits.argmax(dim=1) == labels).sum().item()

        loss.backward()
        optimizer.step()

    train_loss = total_loss / len(train_loader)
    train_acc = total_correct / len(train_dataset)

    model.eval()
    total_correct = 0
    with torch.no_grad():
        for batch in val_loader:
            input_ids = batch["input_ids"].to(device)
            attention_mask = batch["attention_mask"].to(device)
            labels = batch["labels"].to(device)

            outputs = model(input_ids, attention_mask=attention_mask)
            logits = outputs.logits

            total_correct += (logits.argmax(dim=1) == labels).sum().item()

    val_acc = total_correct / len(val_dataset)

    print(f"Epoch {epoch + 1}: Train Loss = {train_loss:.4f}, Train Acc = {train_acc:.4f}, Val Acc = {val_acc:.4f}")

# Save the trained model and tokenizer
model.save_pretrained("models_trained_again/distilbert_sentiment_classifier")
tokenizer.save_pretrained("models_trained_again/distilbert_sentiment_tokenizer")
