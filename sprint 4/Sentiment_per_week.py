import sqlite3
import pandas as pd
import math
from datetime import datetime
import matplotlib.pyplot as plt

conn = sqlite3.connect("../database/tweets.db")

# month 01-12, or empty
month = "12"

# British Airways
if month == "":
    q_compound = """
    SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned == '1'
    """
elif 12 >= int(month) >= 0:
    q_compound = f"""
    SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND British_Airways_mentioned == '1' AND STRFTIME("%m", created_at_datetime) == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")

df_weeklycompound_BA = pd.read_sql_query(q_compound, conn)
df_weeklycompound_BA = df_weeklycompound_BA.groupby("week").mean()

ax_weekly_sentiment_BA = df_weeklycompound_BA.plot(kind="line")
ax_weekly_sentiment_BA.set_ylabel("compound")
ax_weekly_sentiment_BA.set_xlabel("week of the year")
ax_weekly_sentiment_BA.set_title("average weekly sentiment British airways")
plt.show()
ax_weekly_sentiment_BA.get_figure().savefig("figures/weeklysentiment_britishairways.png", transparent=True)

#AmericanAir

if month == "":
    q_compound = """
    SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1'
    """
elif 12 >= int(month) >= 0:
    q_compound = f"""
    SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND AmericanAir_mentioned == '1' AND STRFTIME("%m", created_at_datetime) == "{month}" AND STRFTIME("%m", created_at_datetime) == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")

df_weeklycompound_AA = pd.read_sql_query(q_compound, conn)
df_weeklycompound_AA = df_weeklycompound_AA.groupby("week").mean()

ax_weekly_sentiment_AA = df_weeklycompound_AA.plot(kind="line")
ax_weekly_sentiment_AA.set_ylabel("compound")
ax_weekly_sentiment_AA.set_xlabel("week of the year")
ax_weekly_sentiment_AA.set_title("average weekly sentiment AmericanAir")
plt.show()
ax_weekly_sentiment_AA.get_figure().savefig("figures/weeklysentiment_Americanair.png", transparent=True)

#mean of all

if month == "":
    q_compound = """
    SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id
    """
elif 12 >= int(month) >= 0:
    q_compound = f"""
    SELECT "compound", STRFTIME('%W',created_at_datetime) as "week"
    FROM tweets, sentiment_BERT
    WHERE tweets.id_str = sentiment_BERT.id AND STRFTIME("%m", created_at_datetime) == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")

df_weeklycompound = pd.read_sql_query(q_compound, conn)
df_weeklycompound = df_weeklycompound.groupby("week").mean()

ax_weekly_sentiment = df_weeklycompound.plot(kind="line")
ax_weekly_sentiment.set_ylabel("compound")
ax_weekly_sentiment.set_xlabel("week of the year")
ax_weekly_sentiment.set_title("average weekly sentiment all airways")
plt.show()
ax_weekly_sentiment.get_figure().savefig("figures/weeklysentiment_all_airways.png", transparent=True)



#for all