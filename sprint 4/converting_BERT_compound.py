import sqlite3
import time
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import torch
import torch.nn.functional as F

errors = []


def convert_values_to_probs(values):
    values = values.replace("]", "")
    values = values.replace("[", "")
    values = values.split(" ")
    list_probs1 = []
    for i in values:
        try:
            list_probs1.append(float(i))
        except:
            pass
    try:
        logics = torch.tensor(list_probs1)
        probs = F.softmax(logics, dim=0).tolist()
        probs = str(probs)
        return probs
    except Exception as e:
        errors.append(e)
        return None



def convert_compound_bert(text):
    # print(text)
    # text = text.replace("]", "")
    # text = text.replace("[", "")
    # text = text.split(" ")
    list_probs = eval(text)
    # for i in text:
    #     try:
    #         list_probs.append(float(i))
    #     except:
    #         pass
    max_value = max(list_probs)
    index_max = list_probs.index(max_value)
    sorted_list = []
    for i in list_probs:
        sorted_list.append(i)
    sorted_list.sort()
    second_max_value = sorted_list[1]
    second_max_index = list_probs.index(second_max_value)
    compound = -6
    if index_max == 0:  # Negative
        compound = max_value * -1
    elif index_max == 2:  # Positive
        compound = max_value
    elif index_max == 1:
        if second_max_index == 0:  # Negative neutral
            compound = (1 - max_value) * -1
        elif second_max_index == 2:  # Positive neutral
            compound = 1 - max_value
        else:  # There are 2 exact the same numbers in the list
            index0_value = sorted_list[0]
            index2_value = sorted_list[2]
            if index2_value > index0_value:  # Positive neutral
                compound = 1 - max_value
            else:  # Negative neutral
                compound = (1 - max_value) * -1
    else:
        print(f"MAX INDEX IS NOT 0-2 {index_max}")

    if compound == -6:
        print(f'Compound is -6, list: {list_probs}')
        errors.append(f'Compound is -6, list: {list_probs}')
        compound = None
    else:
        if compound > 1:
            print(f"Compound is above 1: {compound}")
            errors.append(f"Compound is above 1: {compound}")
            compound = 1
        if compound < -1:
            print(f"Compound is below -1: {compound}")
            errors.append(f"Compound is below -1: {compound}")
            compound = -1
    return compound


print("Running SQL query...")

query = """
SELECT id, "values", lang
FROM sentiment
"""
conn = sqlite3.connect("../database/tweets.db")

t = time.time()
begin_time = int(t * 1000)

df_sentiments = pd.read_sql_query(query, conn)

t = time.time()
end_time = int(t * 1000)

print(f"Loaded SQL query (took {((end_time - begin_time) / 1000):.2f} seconds)")
print("Converting values to probabilities...")

t = time.time()
begin_time = int(t * 1000)

df_sentiments['probs'] = df_sentiments['values'].apply(convert_values_to_probs)

t = time.time()
end_time = int(t * 1000)

print(f"Converted values to probabilities (took {((end_time - begin_time) / 1000):.2f} seconds)")

begin_time = end_time
print("Converting probabilities to compounds...")
df_sentiments['compound'] = df_sentiments['probs'].apply(convert_compound_bert)

t = time.time()
end_time = int(t * 1000)

print(f"Converting probabilities to compounds done (took {((end_time - begin_time) / 1000):.2f} seconds)")

print(f"{len(errors)} errors")
if len(errors) != 0:  # Only print error list when there are errors
    print(errors)

df_sentiments.to_parquet("../database/sentiment_BERT_compound.parquet")

print("Saving to SQL....")

t = time.time()
begin_time = int(t * 1000)

table_name = "sentiment_BERT"
try:
    q = f"""
    DROP TABLE {table_name}
    """
    conn.execute(q)
except:
    pass

query = f"""
SELECT *
FROM {table_name}
"""

num_rows_inserted = df_sentiments.to_sql(table_name, conn, index=False)
cursor = conn.execute(query)

t = time.time()
end_time = int(t * 1000)

print(f"Sentiment successfully saved to tweets.db! (took {((end_time - begin_time) / 1000):.2f} seconds)")

print("Plotting sentiment density")

begin_time = end_time
plot = sns.kdeplot(df_sentiments["compound"], bw_method=0.25, color="red")
plot.set_title("Sentiment density", size=24, weight="bold")
plot.get_figure().savefig("figures/sentiment_density.png", transparent=True)
# plot = df_sentiments["compound"].plot(kind="density")
plt.show()

t = time.time()
end_time = int(t * 1000)

print(f"Plotting took {((end_time - begin_time) / 1000):.2f} seconds")
