import gc
import pandas as pd
import sqlite3
import time
import pickle

import functions
import variables
import matplotlib.pyplot as plt

MONTH = ""
month_dct = {"04": "April (undefined Year)", "05": "May 2019", "06": "June 2019", "07": "July 2019", "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020", "": "May 2019 - March 2020"}


pd.options.mode.chained_assignment = None  # Suppress "A value is trying to be set on a copy of a slice from a DataFrame." warning
# pd.options.mode.chained_assignment = 'warn'  # Dont suppress any warnings if needed

conn = sqlite3.connect("../database/tweets.db")

GENERATE_DICTIONARY = False  # Set to True if you want to force generating the dictionary for mining conversations
ITERATIONS_PRINT_PROGRESS = 50000

try:  # Try to get the dictionary from file because that is faster than generating it
    if GENERATE_DICTIONARY:  # If something gives an error here then the code will run the except code (we create an error to find a value with a non-existent index)
        crash_list = []
        _ = crash_list[5]
    with open("../convs/sentiment.pkl", "rb") as sentiment_pickle:
        sentiment_dct = pickle.load(sentiment_pickle)
    with open("../convs/users.pkl", "rb") as users_pickle:
        users_dct = pickle.load(users_pickle)
    with open("../convs/conversations.pkl", "rb") as conversations_pickle:
        conversations_dct = pickle.load(conversations_pickle)
    print("Successfully imported dictionaries from files for faster running time!")

except:
    query_sentiment_user = """
    SELECT compound, user_id_str, id_str
    FROM sentiment_BERT, tweets
    WHERE id == id_str
    """

    query_conversations = """
    SELECT conversation_id, replies
    FROM convos
    """

    df_sentiment_user = pd.read_sql_query(query_sentiment_user, conn)
    df_convos = pd.read_sql_query(query_conversations, conn)

    conversations_dct = {}
    sentiment_dct = {}
    users_dct = {}
    count = 0
    t = time.time()
    begin_time = int(t * 1000)
    length_df_sentiment_user = len(df_sentiment_user.index)
    length_df_convos = len(df_convos.index)

    for row in df_sentiment_user.iterrows():
        sentiment_dct[row[1]["id_str"]] = float(row[1]["compound"])
        users_dct[row[1]["id_str"]] = row[1]["user_id_str"]
        count += 1
        if count % ITERATIONS_PRINT_PROGRESS == 0 or count == length_df_sentiment_user:
            t = time.time()
            end_time = int(t * 1000)
            time_took = (end_time - begin_time) / 1000
            print(
                f"At {(count / length_df_sentiment_user * 100):.2f}%, {ITERATIONS_PRINT_PROGRESS:,} iterations took {time_took:.2f} seconds (still {((length_df_sentiment_user - count) / ITERATIONS_PRINT_PROGRESS * time_took):.2f} seconds to go) for generating dicts for user and sentiment (1/2)")
            begin_time = end_time

    del df_sentiment_user
    gc.collect()

    count = 0
    for row in df_convos.iterrows():
        conversations_dct[row[1]["conversation_id"]] = eval(row[1]["replies"])
        count += 1
        if count % ITERATIONS_PRINT_PROGRESS == 0 or count == length_df_convos:
            t = time.time()
            end_time = int(t * 1000)
            time_took = (end_time - begin_time) / 1000
            print(
                f"At {(count / length_df_convos * 100):.2f}%, {ITERATIONS_PRINT_PROGRESS:,} iterations took {time_took:.2f} seconds (still {((length_df_convos - count) / ITERATIONS_PRINT_PROGRESS * time_took):.2f} seconds to go) for generating dicts for conversations (2/2)")
            begin_time = end_time
    del df_convos
    gc.collect()
    # Save the dictionaries to a file so the next time it can be loaded in from the file which is a lot faster than generating again
    with open("../convs/sentiment.pkl", "wb") as sentiment_pickle:
        pickle.dump(sentiment_dct, sentiment_pickle)
    with open("../convs/users.pkl", "wb") as users_pickle:
        pickle.dump(users_dct, users_pickle)
    with open("../convs/conversations.pkl", "wb") as conversations_pickle:
        pickle.dump(conversations_dct, conversations_pickle)
    print("Successfully saved dictionaries to files in convs folder to save time next time you run this code!")

if MONTH == "":
    query_convos = """
    SELECT conversation_id, replies, airlines_mentioned
    FROM convos
    """
else:
    query_convos = f"""
    SELECT conversation_id, replies, airlines_mentioned
    FROM convos
    WHERE top_tweet IN (SELECT id_str FROM tweets WHERE STRFTIME("%m", created_at_datetime) == '{MONTH}')
    """

df_convos_sentiment = pd.read_sql_query(query_convos, conn)


def get_sentiment_change(replies):
    replies = eval(replies)
    try:
        if users_dct[replies[0]] == users_dct[replies[-1]]:
            sentiment_change = sentiment_dct[replies[-1]] - sentiment_dct[replies[0]]
            return sentiment_change
        elif users_dct[replies[0]] == users_dct[replies[-2]]:
            sentiment_change = sentiment_dct[replies[-2]] - sentiment_dct[replies[0]]
            return sentiment_change
        else:
            return None
    except:
        return None

sentiment_change_dct = []
print(len(variables.AIRLINES.values())-2)
fig, ax = plt.subplots(nrows=4, ncols=3, figsize=(15, 15), sharex=True, sharey=True)
count = 0
dct_position = {0: ax[0, 0], 1: ax[1,0], 2: ax[2, 0], 3: ax[3,0], 4: ax[0, 1], 5: ax[1,1], 6: ax[2, 1], 7: ax[3,1], 8: ax[0, 2], 9: ax[1,2], 10: ax[2,2]}
for airline in variables.AIRLINES.values():
    if airline != 26223583 and airline != 2182373406:  # Dismiss Airberlin and Airberlin_assist because of the very low amount of tweets
        df_airline = df_convos_sentiment[df_convos_sentiment["airlines_mentioned"] == f"[{str(airline)}]"]
        df_airline["sentiment_change"] = df_airline["replies"].apply(get_sentiment_change)
        df_airline = df_airline[~df_airline["sentiment_change"].isna()]
        plot = df_airline["sentiment_change"].plot(kind="density", ax=dct_position[count])
        count += 1
        plot.set_title(f"{variables.AIRLINES_INVERSED[airline]} sentiment change")
        plot.set_xlim(-2,2)
        sentiment_change_mean = df_airline["sentiment_change"].mean()
        number_sentiment_changes = len(df_airline.index)
        # print(f"{variables.AIRLINES_INVERSED[airline]} has a sentiment change mean of {sentiment_change_mean:.2f} ({number_sentiment_changes:,} sentiment changes)")
        sentiment_change_dct.append({"airline": variables.AIRLINES_INVERSED[airline], "sentiment_change": sentiment_change_mean})
fig.suptitle("Sentiment change per airline", size=36, weight="bold")
plt.savefig("figures/BERT_density_sentiment_change_airlines.png", transparent=True)
plt.show()
df_sentiment_changed_airlines = pd.DataFrame(sentiment_change_dct)
df_sentiment_changed_airlines = df_sentiment_changed_airlines.sort_values("sentiment_change", ascending=False)
print(df_sentiment_changed_airlines)
plot_sentiment = df_sentiment_changed_airlines.plot(kind="bar", x="airline", y="sentiment_change", figsize=(12, 8))
plot_sentiment.get_legend().remove()
plot_sentiment.set_title(f"Mean sentiment change in conversation per airline\nin {month_dct[MONTH]}", size=24, weight="bold")
plot_sentiment.set_xlabel("Airline", size=20)
plot_sentiment.set_ylabel("Mean sentiment change", size=20)
plot_sentiment.set_ylim(0.13, 0.19)
plot_sentiment.tick_params(labelsize=20)
plt.tight_layout()
plt.xticks(rotation=45)

plot_sentiment.get_figure().savefig("figures/BERT_sentiment_change_airline.png", transparent=True)
print("Saved plot to figures/BERT_sentiment_change_airline.png")
functions.open_image("figures/BERT_sentiment_change_airline.png")
try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")
