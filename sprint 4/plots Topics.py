import numpy as np
import pandas as pd
from pandas.plotting import table
import sqlite3
import warnings
import matplotlib
import matplotlib.pyplot as plt

import functions

plt.style.use('tableau-colorblind10')

warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)

plt.rcParams["figure.dpi"] = 300

conn = sqlite3.connect("../database/tweets.db")

# month 01-12, or empty
month: str = ""
month_dct = {"04": "April (undefined Year)", "05": "May 2019", "06": "June 2019", "07": "July 2019", "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020", "": "May 2019 - March 2020"}


if month == "":
    query_top5_topics = """
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic
    FROM tweets, topic_model0
    WHERE tweets.id_str == topic_model0.id_str AND (British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1')
    UNION ALL
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic
    FROM tweets, topic_competitors_model0
    WHERE tweets.id_str == topic_competitors_model0.id_str AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
    """
elif 12 >= int(month) >= 0:
    query_top5_topics = f"""
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic
    FROM tweets, topic_model0
    WHERE tweets.id_str == topic_model0.id_str AND (British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1') AND "month" == "{month}"
    UNION ALL
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic
    FROM tweets, topic_competitors_model0
    WHERE tweets.id_str == topic_competitors_model0.id_str AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')  AND "month" == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")

df_top5_topics = pd.read_sql_query(query_top5_topics, conn)
df_top5_topics = df_top5_topics[df_top5_topics["topic"] != "n.d."]
df_top5_topics["amount"] = 1
df_top5_topics = df_top5_topics[["topic", "amount"]].copy().groupby("topic").sum().sort_values(by=["amount"], ascending=False)
df_top5_topics = df_top5_topics[:5].reset_index()
top_5_topics = df_top5_topics["topic"].values.tolist()
print(df_top5_topics)


# British Airways
if month == "":
    query_topics_month = f"""
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic, compound
    FROM tweets, topic_model0, sentiment_BERT
    WHERE tweets.id_str == topic_model0.id_str AND tweets.id_str == sentiment_BERT.id AND British_Airways_mentioned == '1'
    """
elif 12 >= int(month) >= 0:
    query_topics_month = f"""
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic, compound
    FROM tweets, topic_model0, sentiment_BERT
    WHERE tweets.id_str == topic_model0.id_str AND tweets.id_str == sentiment_BERT.id AND British_Airways_mentioned == '1' AND "month" == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")


df_topics_month = pd.read_sql_query(query_topics_month, conn)
length = len(df_topics_month.index)
df_topics_month = df_topics_month[(df_topics_month["topic"] == top_5_topics[0]) | (df_topics_month["topic"] == top_5_topics[1]) | (df_topics_month["topic"] == top_5_topics[2]) | (df_topics_month["topic"] == top_5_topics[3]) | (df_topics_month["topic"] == top_5_topics[4])]
df_topics_month["amount"] = 1
# df_sentiment_ba = df_topics_month.copy()
# df_sentiment_ba["sentiment"] = df_sentiment_ba["probs"].apply(probs_to_sentiment)
df_topics_month = df_topics_month[["topic", "amount", "compound"]].copy().groupby("topic").agg({"amount": "sum", "compound": "mean"}).reset_index()
df_topics_month["%"] = 100*df_topics_month["amount"]/length

df = pd.DataFrame()
for i in top_5_topics:
    if i in df_topics_month["topic"].values:
        df1 = df_topics_month[df_topics_month["topic"] == i]
        df = pd.concat([df, df1], ignore_index=True)
    else:
        df1 = pd.DataFrame([[i, 0, 0, "NaN"]], columns=["topic", "amount", "%", "compound"])
        df = pd.concat([df, df1], ignore_index=True)

df_topics_ba = df.copy()
df_topics_month = df.copy()
df_topics_month = df_topics_month[:5].set_index("topic")
print(df_topics_month)


# American Air
if month == "":
    query_topics_month = """
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic, compound
    FROM tweets, topic_model0, sentiment_BERT
    WHERE tweets.id_str == topic_model0.id_str AND tweets.id_str == sentiment_BERT.id AND AmericanAir_mentioned == '1'
    """
elif 12 >= int(month) >= 0:
    query_topics_month = f"""
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic, compound
    FROM tweets, topic_model0, sentiment_BERT
    WHERE tweets.id_str == topic_model0.id_str AND tweets.id_str == sentiment_BERT.id AND AmericanAir_mentioned == '1' AND "month" == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")


df_topics_month = pd.read_sql_query(query_topics_month, conn)
length = len(df_topics_month.index)
df_topics_month = df_topics_month[(df_topics_month["topic"] == top_5_topics[0]) | (df_topics_month["topic"] == top_5_topics[1]) | (df_topics_month["topic"] == top_5_topics[2]) | (df_topics_month["topic"] == top_5_topics[3]) | (df_topics_month["topic"] == top_5_topics[4])]
df_topics_month["amount"] = 1
# df_sentiment_aa = df_topics_month.copy()
# df_sentiment_aa["sentiment"] = df_sentiment_aa["probs"].apply(probs_to_sentiment)
df_topics_month = df_topics_month[["topic", "amount", "compound"]].copy().groupby("topic").agg({"amount": "sum", "compound": "mean"}).reset_index()
df_topics_month["%"] = 100*df_topics_month["amount"]/length

df = pd.DataFrame()
for i in top_5_topics:
    if i in df_topics_month["topic"].values:
        df1 = df_topics_month[df_topics_month["topic"] == i]
        df = pd.concat([df, df1], ignore_index=True)
    else:
        df1 = pd.DataFrame([[i, 0, 0, "NaN"]], columns=["topic", "amount", "%", "compound"])
        df = pd.concat([df, df1], ignore_index=True)

df_topics_aa = df.copy()
df_topics_month = df.copy()
df_topics_month = df_topics_month[:5].set_index("topic")
print(df_topics_month)


# all other competitors
if month == "":
    query_topics_month = """
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic, compound
    FROM tweets, topic_competitors_model0, sentiment_BERT
    WHERE tweets.id_str == topic_competitors_model0.id_str AND tweets.id_str == sentiment_BERT.id AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
    """
elif 12 >= int(month) >= 0:
    query_topics_month = f"""
    SELECT STRFTIME("%m", created_at_datetime) AS "month", New_Topic AS topic, compound
    FROM tweets, topic_competitors_model0, sentiment_BERT
    WHERE tweets.id_str == topic_competitors_model0.id_str AND tweets.id_str == sentiment_BERT.id AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1') AND "month" == "{month}"
    """
else:
    assert 1 == 2, Exception("'month' is not a number (string) between 1 and 12 or an empty string.")


df_topics_month = pd.read_sql_query(query_topics_month, conn)
length = len(df_topics_month.index)
df_topics_month = df_topics_month[(df_topics_month["topic"] == top_5_topics[0]) | (df_topics_month["topic"] == top_5_topics[1]) | (df_topics_month["topic"] == top_5_topics[2]) | (df_topics_month["topic"] == top_5_topics[3]) | (df_topics_month["topic"] == top_5_topics[4])]
df_topics_month["amount"] = 1
# df_sentiment_oc = df_topics_month[["topic", "probs", "amount"]].copy()
# df_sentiment_oc["sentiment"] = df_sentiment_oc["probs"].apply(probs_to_sentiment)
# df_sentiment_oc = df_sentiment_oc[["topic", "sentiment"]].groupby(["topic", "sentiment"]).size()
# print(df_sentiment_oc)
df_topics_month = df_topics_month[["topic", "amount", "compound"]].copy().groupby("topic").agg({"amount": "sum", "compound": "mean"}).reset_index()
df_topics_month["%"] = 100*df_topics_month["amount"]/length

df = pd.DataFrame()
for i in top_5_topics:
    if i in df_topics_month["topic"].values:
        df1 = df_topics_month[df_topics_month["topic"] == i]
        df = pd.concat([df, df1], ignore_index=True)
    else:
        df1 = pd.DataFrame([[i, 0, 0, "NaN"]], columns=["topic", "amount", "%", "compound"])
        df = pd.concat([df, df1], ignore_index=True)

df_topics_oc = df.copy()
df_topics_month = df.copy()
df_topics_month = df_topics_month[:5].set_index("topic")
print(df_topics_month)

# create dataframe for table
zipped_data = zip(df_topics_ba["compound"].values.tolist(), df_topics_aa["compound"].values.tolist(), df_topics_oc["compound"].values.tolist())
zipped_lst = []
for ba, aa, oc in zipped_data:
    print(ba, aa, oc)
    # zipped_lst.append(f"{'{:.2f}'.format(ba)} | {'{:.2f}'.format(aa)} | {'{:.2f}'.format(oc)}")
    try:
        ba0 = "{:.2f}".format(ba)
    except:
        ba0 = ba
    try:
        aa0 = "{:.2f}".format(aa)
    except:
        aa0 = aa
    try:
        oc0 = "{:.2f}".format(oc)
    except:
        oc0 = oc
    zipped_lst.append(f"{ba0} | {aa0} | {oc0}")
    # zipped_lst.append("{:.2f}".format(ba))
    # zipped_lst.append("{:.2f}".format(aa))
    # zipped_lst.append("{:.2f}".format(oc))
df_compound = pd.DataFrame([zipped_lst], index=["mean compound"], columns=top_5_topics)
print(df_compound)

# create the plot
x = np.arange(5)
bar_w = 0.28

fig, ax = plt.subplots(figsize=(13, 6.5))

bars_ba = ax.bar(x, df_topics_ba["%"].values.tolist(), bar_w, label="British Airways")
bars_aa = ax.bar(x + bar_w, df_topics_aa["%"].values.tolist(), bar_w, label="American Airlines")
bars_oc = ax.bar(x + 2*bar_w, df_topics_oc["%"].values.tolist(), bar_w, label="other competitors")


def bar_labeler(bars, labels0):
    labels = []
    for label in labels0:
        labels.append(f"{label}")
    for bar, label in zip(bars, labels):
        height = bar.get_height()
        ax.annotate(label,
                    xy=(bar.get_x() + bar.get_width() / 2, height),
                    xytext=(0, 3),
                    textcoords="offset points",
                    ha="center", va="bottom", fontsize=11,
                    rotation=45)


bar_labeler(bars_ba, df_topics_ba["amount"].values.tolist())
bar_labeler(bars_aa, df_topics_aa["amount"].values.tolist())
bar_labeler(bars_oc, df_topics_oc["amount"].values.tolist())

ax.set_xlabel("Topics", size=16, labelpad=34)
ax.set_ylabel("Number of tweets (%)", size=16)
ax.set_title(f"Top 5 topics during {month_dct[str(month)]}", weight="bold", size=24)
# ax.set_xticks(x + bar_w)
# ax.set_xticklabels(top_5_topics)
plt.tick_params(
    axis='x',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    bottom=False,      # ticks along the bottom edge are off
    top=False,         # ticks along the top edge are off
    labelbottom=False) # labels along the bottom edge are off

plt.tick_params(axis="y",
                labelsize=12)

if month != "":
    ax.set_ylim(0, 25)
else:
    ax.set_ylim(0, 12)
ax.legend()
# table(ax, df_compound, loc="bottom")
# ax.axis("tight")
# ax.axis("off")
the_table = ax.table(cellText=[zipped_lst],
                      rowLabels=["mean compound"],
                      colLabels=top_5_topics,
                      cellLoc="center",
                      loc='bottom')
the_table.auto_set_font_size(False)
the_table.set_fontsize(12)
the_table.scale(1,1.2)

# plt.subplots_adjust(hspace=0.8)
# plt.tight_layout()
fig.get_figure().savefig("figures/topics/topics_top5.png", transparent=True)
print("Plot successfully saved to figures/topics/topics_top5.png")
functions.open_image("figures/topics/topics_top5.png")
try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")

