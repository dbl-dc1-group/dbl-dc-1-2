import pickle
import emot
import pandas as pd
from emot.emo_unicode import UNICODE_EMOJI
from emot.emo_unicode import EMOTICONS_EMO
import sqlite3
import time

conn = sqlite3.connect("../database/tweets.db")
query_for_emoji_transformation = """
SELECT translation
FROM tweets_en
"""
df_temp = pd.read_sql_query(query_for_emoji_transformation, conn)
df_for_transforming_emojis = df_temp.copy()

emoji_mapping = {**UNICODE_EMOJI, **EMOTICONS_EMO}
print(len(emoji_mapping))

def convert_emojis_and_emoticons(text):
    words = text.split()
    transformed_words = []
    for word in words:
        transformed_word = emoji_mapping.get(word, word)
        if transformed_word == word:
            for emoji in emoji_mapping:
                if emoji in word:
                    transformed_word = emoji_mapping[emoji]
                    break
        transformed_words.append(transformed_word)
    return ' '.join(transformed_words)


t = time.time()
totalbegin_time = int(t * 1000)

# Calculate length of the DataFrame subset
calc_length_df = len(df_for_transforming_emojis["translation"]) + 1

# Set progress variables
progressie_to_print = 0.001
progress_counter = int(calc_length_df * progressie_to_print)

# Initialize an empty list to store transformed texts
transformed_texts = []

for i, tweet in enumerate(df_for_transforming_emojis["translation"], start=1):
    t = time.time()
    begin_time = int(t * 1000)

    # Convert emojis and emoticons in the tweet
    transformed_tweet = convert_emojis_and_emoticons(tweet)
    transformed_texts.append(transformed_tweet)

    t = time.time()
    end_time = int(t * 1000)
    elapsed_time = (end_time - begin_time) / 1000

    # Calculate remaining time
    remaining_tweets = len(df_for_transforming_emojis) - i
    remaining_time = remaining_tweets * elapsed_time / 3600  # Convert to hours

    # Print progress and remaining time
    if i >= progress_counter:
        progress = (i / calc_length_df) * 100
        print(f"Progress in percentage: {progress:.2f}%")
        print(f"Time remaining: {remaining_time:.2f} hours")
        progress_counter += int(calc_length_df * progressie_to_print)

t = time.time()
totalend_time = int(t * 1000)
accurate_time = (totalend_time - totalbegin_time) / 1000
accurate_time_minutes = accurate_time / 60

print(f"Time took: {accurate_time:.2f} seconds")
print(f"Time took: {accurate_time_minutes:.2f} minutes")

# Create a new DataFrame with the transformed texts
df_transformed = pd.DataFrame()
df_transformed["text_transformed_emojis"] = transformed_texts

# Save the DataFrame as a Parquet file
df_transformed.to_parquet("../database/tweets_WITHOUT_EMOJI_tweets_en.parquet", engine='pyarrow')
# del df_for_test
# del df_for_transforming_emojis
# df_read = pd.read_parquet("../database/tweets_WITHOUT_EMOJI.parquet")

# table_name = "tweets"
# conn = sqlite3.connect("../database/tweets.db")
# existing_table_df = pd.read_sql_query('SELECT * FROM tweets', conn)
# existing_table_df["text_WITHOUTEMOJI"] = df_read["text_transformed_emojis"]
# try:
#    q = f"""
#    DROP TABLE {table_name}
#    """
#    conn.execute(q)
# except:
#    pass

# num_rows_inserted = df_read.to_sql(table_name, conn, index=False)
# try:
#    cursor = conn.execute(query)
# except:
#    print("Kon je ook weglaten")
# print('Tweets successfully saved to tweets.db!')
