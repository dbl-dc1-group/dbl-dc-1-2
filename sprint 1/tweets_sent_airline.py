import pandas as pd
import sqlite3
import matplotlib.pyplot as plt
import variables

conn = sqlite3.connect("../database/tweets.db")

month_dct = {"04": "April (undefined Year)", "05": "May 2019", "06": "June 2019", "07": "July 2019", "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020", "": "May 2019 - March 2020"}


MONTH = ""
select_list = []
for airline in variables.AIRLINES.keys():
    select_list.append(f"sum({airline}_mentioned) as {airline}")


if MONTH == "":
    query_sent_airlines = f"""
    SELECT user_id_str
    FROM tweets
    """
else:
    query_sent_airlines = f"""
    SELECT user_id_str
    FROM tweets
    WHERE STRFTIME("%m", created_at_datetime) == '{MONTH}' 
    """

df_sent = pd.read_sql_query(query_sent_airlines, conn)
dct_sent = {}
for airline in variables.AIRLINES.items():
    if airline[0] != "AirBerlin" and airline[0] != "AirBerlin_assist":
        dct_sent[airline[0]] = len(df_sent[df_sent["user_id_str"] == str(airline[1])].index)

df_sent = pd.DataFrame(list(dct_sent.items()), columns=["airline", "amount"])
df_sent = df_sent.sort_values("amount", ascending=False)
plot_sent = df_sent.plot(kind="bar", x="airline", y="amount", figsize=(12,8))
plot_sent.set_title(f"Tweets sent per airline in {month_dct[MONTH]}", size=24, weight="bold")
plot_sent.set_xlabel("Airline", size=20)
plot_sent.set_ylabel("Number of Tweets", size=20)
plot_sent.tick_params(labelsize=20)
plot_sent.get_legend().remove()
plt.grid(False)
plt.tight_layout(pad=3)
plt.xticks(rotation=45)
plot_sent.get_figure().savefig("figures/tweets_send.png", transparent=True)
plt.show()
