import pandas as pd
import sqlite3
import matplotlib.pyplot as plt

conn = sqlite3.connect("../database/tweets.db")

query_bots_British = """
SELECT user_id_str, British_Airways_mentioned
FROM tweets
WHERE user_statuses_count > 50000 AND user_friends_count > 100 AND user_followers_count < 10 AND British_Airways_mentioned = 1 
"""
df_bots_British = pd.read_sql_query(query_bots_British, conn)


query_bots_American = """
SELECT user_id_str, AmericanAir_mentioned
FROM tweets
WHERE user_statuses_count > 50000 AND user_friends_count > 100 AND user_followers_count < 10 AND AmericanAir_mentioned = 1 
"""
df_bots_American = pd.read_sql_query(query_bots_American, conn)

query_British = """
SELECT user_id_str, British_Airways_mentioned
FROM tweets
WHERE British_Airways_mentioned = 1 
"""
df_British = pd.read_sql_query(query_British, conn)


query_American = """
SELECT user_id_str, AmericanAir_mentioned
FROM tweets
WHERE AmericanAir_mentioned = 1 
"""
df_American = pd.read_sql_query(query_American, conn)
print(f"len of British bots: {len(df_bots_British)}")
print(f"len of British tweets: {len(df_British)}")
print(f"len of American bots: {len(df_bots_American)}")
print(f"len of American tweets: {len(df_American)}")

ratio_bots_British = (len(df_bots_British) / len(df_British)) * 100
ratio_bots_American = (len(df_bots_American) / len(df_American)) * 100
print(ratio_bots_British)
print(ratio_bots_American)
print(f"ratio bots British: {ratio_bots_British}")
print(f"ratio bots American: {ratio_bots_American}")

# Set the data
british_bots = len(df_bots_British)
british_tweets = len(df_British)
american_bots = len(df_bots_American)
american_tweets = len(df_American)

# Create a bar chart
fig, ax = plt.subplots(ncols = 3, nrows = 1, figsize=(21,5))
ax[0].bar(["British", "American"], [ratio_bots_British, ratio_bots_American], color= ["blue", "red"])

# Set the y-axis label and title
ax[0].set_ylabel("Ratio of Bots to Tweets")
ax[0].set_title("Comparison of Bot-to-Tweet Ratio")

ax[1].bar(["British", "American"], [british_tweets, american_tweets], color= ["blue", "red"])

# Set the y-axis label and title
ax[1].set_ylabel("Number of Tweets")
ax[1].set_title("Comparison of Total Tweets")

ax[2].bar(["British", "American"], [british_bots, american_bots], color= ["blue", "red"])

# Set the y-axis label and title
ax[2].set_ylabel("Number of Bot Tweets")
ax[2].set_title("Comparison of Total Bot Tweets")
fig.savefig("figures/bots.svg")
plt.show()
