import gc
import pandas as pd
from variables import LENGTH_JSON


def check_files():
    df = pd.read_parquet("database/airlines_db.parquet")
    length = len(df.index)
    del df
    print(
        f"Are the lengths of the parquet and the jsons equal? {LENGTH_JSON == length} (Length json: {LENGTH_JSON}; Length parquet database: {length})")


def check_top_tweets(conn):

    query = """
    SELECT id_str, in_reply_to_status_id_str
    FROM tweets
    WHERE in_reply_to_status_id_str == 'None'
    """

    df_top_tweets = pd.read_sql_query(query, conn)
    top_tweets_dict = {}
    count = 0
    length = len(df_top_tweets.index)
    for row in df_top_tweets.iterrows():
        top_tweets_dict[row[1]['id_str']] = row[1]["in_reply_to_status_id_str"]
        count += 1
        if count % 100000 == 0:
            print(f"At {(count / length * 100):.2f}% making dictionary for checking top tweets")
    del df_top_tweets
    gc.collect()

    query_top_tweets = """
    SELECT top_tweet
    FROM convos
    """

    df_top_tweets = pd.read_sql_query(query_top_tweets, conn)
    replies = list(df_top_tweets["top_tweet"].values)
    del df_top_tweets
    gc.collect()
    count_error = 0
    non_top_tweets = []
    for reply in replies:
        try:
            _ = top_tweets_dict[reply]
        except:
            print(f"Tweet {reply} is not top tweet")
            count_error += 1
            non_top_tweets.append(reply)
    if count_error == 0:
        print(f"All main/top tweets in convos table are actually main/top tweets")
    else:
        print(f"The following {count_error} tweets are no top tweets: {non_top_tweets}")