import pandas as pd
import time
import sqlite3
from transformers import AutoTokenizer
from transformers import TFAutoModelForSequenceClassification
from transformers import AutoConfig
import tensorflow as tf
import numpy as np
from scipy.special import softmax
import torch
if tf.test.is_gpu_available():
    # Set TensorFlow to use GPU
    tf.config.experimental.set_memory_growth(tf.config.list_physical_devices('GPU')[0], True)
conn = sqlite3.connect("../sent_ana_test/database_sent_ana.sqlite")

query_for_SA = """
SELECT text, airline_sentiment 
FROM Tweets
"""

df_tweets_at_ba = pd.read_sql_query(query_for_SA, conn)
df_tweets_at_ba_copy = df_tweets_at_ba.copy(deep= True)
df_tweets_for_testing_code = df_tweets_at_ba_copy.iloc[:1]
t = time.time()
config = AutoConfig.from_pretrained("cardiffnlp/twitter-roberta-base-sentiment")
tokenizer = AutoTokenizer.from_pretrained("cardiffnlp/twitter-roberta-base-sentiment")
model = TFAutoModelForSequenceClassification.from_config(config)

sentiment_labels = ["positive", "neutral", "negative"]
t = time.time()
begin_time = int(t*1000)
positive_threshold = 0.7
negative_threshold = 0.3
labled_tweets = []
indexfortime = 0
for i in df_tweets_for_testing_code["text"]:
    indexfortime += 1
    tokenized_tweet = tokenizer(i, return_tensors="tf")
    #print(tokenized_tweet)
    classified_tweet = model(tokenized_tweet)
    prob1 = classified_tweet[0][0].numpy()
    sentiment_probability = softmax(prob1)
    print(f"Time needed to finish in percentage: {(indexfortime/len(df_tweets_for_testing_code))*100}%")
    index = np.argmax(sentiment_probability)
    sentiment2 = sentiment_labels[index]
    labled_tweets.append(sentiment2)
t = time.time()
end_time = int(t*1000)
accurate_time = ((end_time-begin_time)/1000)


# Define the sentiment labels
i=0
z = 0
for tweet, sentiment in zip(df_tweets_for_testing_code["text"], labled_tweets):
    print(sentiment, df_tweets_at_ba_copy['airline_sentiment'].iloc[i])
    if sentiment == df_tweets_at_ba_copy['airline_sentiment'].iloc[i]:
        z += 1
    i += 1
print(f"accuracy = {(z/i)*100}")
print(f"time took:{accurate_time:.2f} seconds")
print(f"time needed:{(accurate_time/len(df_tweets_for_testing_code)*len(df_tweets_at_ba_copy)):.2f} seconds")
print(f"time needed:{(accurate_time/len(df_tweets_for_testing_code)*len(df_tweets_at_ba_copy))/60:.2f} minutes")
# Set the threshold for positive and negative sentiments


# Convert BERT output to sentiment labels

x = "I like you. I love you"
tokenized_tweet = tokenizer(x, return_tensors="tf")
classified_tweet = model(tokenized_tweet)
prob1 = classified_tweet[0][0].numpy()
sentiment_probability = softmax(prob1)
print(f"Time needed to finish in percentage: {(indexfortime / len(df_tweets_for_testing_code)) * 100}%")
index = np.argmax(sentiment_probability)
sentiment2 = sentiment_labels[index]
labled_tweets.append(sentiment2)
print(f"test analysis: {sentiment2}, Positive")
