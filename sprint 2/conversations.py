import math
import sqlite3
import time
import pandas as pd
import pickle
import checks
import variables

MINIMAL_CONVO_LENGTH = 3
MAXIMAL_CONVO_LENGTH = 40
GENERATE_DICTIONARY = False  # Set to True if you want to force generating the dictionary for mining conversations
THRESHOLD_MISSING_REPLIES_PERCENTAGE = 10  # Percentage of missing values (set value to percentage as integer value)
THRESHOLD_MISSING_REPLIES = 2  # How many replies can be missing when the conversation length is 10 or smaller

conn = sqlite3.connect("../database/tweets.db")

query_lat_replies = """
SELECT id_str, in_reply_to_status_id_str, user_id_str
FROM tweets
WHERE id_str NOT IN (SELECT in_reply_to_status_id_str FROM tweets) AND in_reply_to_status_id_str != 'None'
"""
df_last_replies = pd.read_sql_query(query_lat_replies, conn)
try:  # Try to get the dictionary from file because that is faster then generating it
    if GENERATE_DICTIONARY:  # If something gives an error here then the code will run the except code (we create an error to find a value with a non-existent index)
        crash_list = []
        _ = crash_list[5]
    with open("../convs/tweets_for_replies.pkl", "rb") as tweet_pickle:
        tweets = pickle.load(tweet_pickle)
    with open("../convs/mentions.pkl", "rb") as mentions_pickle:
        mentions = pickle.load(mentions_pickle)
    with open("../convs/users.pkl", "rb") as users_pickle:
        users_dct = pickle.load(users_pickle)

except:  # No dictionary file is found so generate it or you forced to make the dictionary
    query_tweets = """
    SELECT id_str, in_reply_to_status_id_str, mention_id_str
    FROM tweets
    """

    df_tweets = pd.read_sql_query(query_tweets, conn)
    count_dict = 0
    tweets = {}
    mentions = {}
    users_dct = {}
    # Make a dictionary with id_str as key and in_reply_to_status_id_str as value to mine conversations
    for item in df_tweets.iterrows():
        count_dict += 1
        tweets[item[1]["id_str"]] = item[1]["in_reply_to_status_id_str"]
        users_dct[item[1]["id_str"]] = item[1]["user_id_str"]
        if item[1]["mention_id_str"] == "0":
            mentions[item[1]["id_str"]] = ["0"]
        else:
            mentions_list = eval(item[1]["mention_id_str"])
            mentions[item[1]["id_str"]] = mentions_list
        if count_dict % 100000 == 0:  # Print progress (check if count_dict is dividable by 100000 so there is no spam of progress reports)
            print(
                f"At {(count_dict / len(df_tweets.index) * 100):.2f}% with creating dictionary with ids and reply ids")
    # Save dictionary to
    with open("../convs/tweets_for_replies.pkl", "wb") as tweet_pickle:
        pickle.dump(tweets, tweet_pickle)
    with open("../convs/mentions.pkl", "wb") as mentions_pickle:
        pickle.dump(mentions, mentions_pickle)
    with open("../convs/users.pkl", "wb") as users_pickle:
        pickle.dump(users_dct, users_pickle)

print("Beginning conversation mining...")

last_replies_list = list(df_last_replies["id_str"].values)
replies_list = []
conversations = []


# Function that gets a Tweet id of last reply in conversation and 'mines' to the top Tweet (Tweet that is no reply)
def conversation_mine_up(id):
    try:
        replies_list.append(id)
        return conversation_mine_up(tweets[id])
    except:
        return replies_list


conversation = []
count = 0
clean_count = 0
t = time.time()
begin_time = int(t * 1000)

query_top_tweets = """
SELECT id_str
FROM tweets
WHERE in_reply_to_status_id_str == 'None'
"""
df_top_tweets = pd.read_sql_query(query_top_tweets, conn)
top_tweets_ids = list(df_top_tweets["id_str"].values)
del df_top_tweets
top_tweets_dict = dict.fromkeys(top_tweets_ids, "1")
del top_tweets_ids
# Give each id of last reply in conversation to the conversation mine function

error_count = 0
airline_involved = False
airline_ids = []
for airline in variables.AIRLINES.values():
    airline_ids.append(str(airline))

for id in last_replies_list:
    airlines_tweeted = []
    airlines_mentioned = []
    airline_involved = False
    remove_convo = False
    missing_replies = 0
    mentioned_dct = {}
    replies_list = conversation_mine_up(id)
    if replies_list[-1] == "None":  # If the function gives the top tweet id 'None' then the top tweet exists in tweets.db otherwise it doesn't exist so go to the next loop
        del replies_list[-1]
    else:
        continue
    if MINIMAL_CONVO_LENGTH <= len(
            replies_list) <= MAXIMAL_CONVO_LENGTH:  # Check if conversation is not too long and not too short
        conversation_id = abs(hash(tuple(replies_list)))
        convo_dict = {"conversation_id": conversation_id, "replies": str(list(reversed(replies_list))),
                      "top_tweet": list(reversed(replies_list))[0], "length_convo": len(replies_list)}
        for reply in replies_list:
            try:
                if users_dct[reply] in airline_ids:
                    airlines_tweeted.append(int(users_dct[reply]))
                    airline_involved = True
            except:
                pass
            for airline in variables.AIRLINES.items():
                try:
                    if str(airline[1]) in mentions[reply]:
                        mentioned_dct[f"{airline[0]}_mentioned"] = "1"
                        airlines_mentioned.append(airline[1])
                        airline_involved = True
                    else:
                        mentioned_dct[f"{airline[0]}_mentioned"] = "0"
                except:
                    pass
        if len(airlines_mentioned) == 0:
            airlines_mentioned = [0]
        if len(airlines_tweeted) == 0:
            airlines_tweeted = [0]
        convo_dict["airlines_mentioned"] = str(list(set(airlines_mentioned)))
        convo_dict["airlines_tweeted"] = str(list(set(airlines_tweeted)))
        # Check if there are not too much missing replies in the tweets.db database
        if len(replies_list) == MINIMAL_CONVO_LENGTH:
            threshold_missing_replies_dynamic = 0
        elif len(replies_list) <= 5:
            threshold_missing_replies_dynamic = 1
        elif len(replies_list) <= 10:
            threshold_missing_replies_dynamic = THRESHOLD_MISSING_REPLIES
        else:
            threshold_missing_replies_dynamic = math.floor(len(replies_list)/THRESHOLD_MISSING_REPLIES_PERCENTAGE)
        if airline_involved and missing_replies <= threshold_missing_replies_dynamic:
            final_dict = convo_dict | mentioned_dct
            conversations.append(final_dict)
            clean_count += 1
        else:
            error_count += 1
    count += 1
    replies_list = []
    if count % 100000 == 0:  # Print progress if count is dividable by 100000 (to avoid spam of progress reports)
        t = time.time()
        end_time = int(t * 1000)
        time_took = (end_time - begin_time) / 1000
        print(
            f"At {(count / len(last_replies_list) * 100):.2f}% mining conversations, took {time_took:.2f} seconds for 100,000 iterations (still {((len(last_replies_list) - count) / 100000 * time_took):.2f} seconds to go) ({error_count} conversations skipped due to one or more replies not in database and/or airline is not mentioned)")
        begin_time = end_time

# Generate a dataframe from the conversations
df_convos = pd.DataFrame(conversations)

print(
    f"Gone from {len(last_replies_list):,} to {clean_count:,} conversations ({abs((clean_count - len(last_replies_list)) / len(last_replies_list) * 100):.2f}% reduction) (minimal conversation length: {MINIMAL_CONVO_LENGTH}, maximal conversation length: {MAXIMAL_CONVO_LENGTH}) {error_count} conversations skipped due to one or more replies not in database and/or airline is not mentioned")
print("Transferring parquet file into SQlite (database/tweets.db)")

table_name = "convos"

try:  # Try to delete convos table, SQL only can make a table if the table doesn't already exist
    q = f"""
    DROP TABLE {table_name}
    """
    conn.execute(q)
except:  # The table doesn't exist so no need to delete the table
    pass

query = f"""
SELECT *
FROM {table_name}
"""

# Insert the convos table in tweets.db
num_rows_inserted = df_convos.to_sql(table_name, conn, index=False)
cursor = conn.execute(query)
print("Successfully mined and saved conversations! See convos table in database/tweets.db for the conversations.")
print("Checking if top tweets are actually top tweets...")

# Check if the top tweet is actually the top tweet
checks.check_top_tweets(conn)
