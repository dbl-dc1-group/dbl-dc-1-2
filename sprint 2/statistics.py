import datetime
import sqlite3
import pandas as pd
from dateutil import parser
import matplotlib.pyplot as plt
import variables
from variables import AIRLINES

conn = sqlite3.connect("../database/tweets.db")

lst_airlines = []
for i in AIRLINES.items():
    lst_airlines.append(i[0])

str_airlines = ""
count = 0
for i in lst_airlines:
    count += 1
    if count < len(lst_airlines):
            str_airlines += f"{i}_mentioned == '1' OR "
    elif count == len(lst_airlines):
        str_airlines += f"{i}_mentioned == '1'"

print(lst_airlines)
print(str_airlines)

query_percentage_airlines = f"""
SELECT COUNT(created_at) AS "times mentioned"
FROM tweets
WHERE {str_airlines}
UNION ALL
SELECT count(created_at) AS "times mentioned"
FROM tweets
WHERE British_Airways_mentioned == 1
UNION ALL
SELECT count(created_at) AS "times mentioned"
FROM tweets
WHERE AmericanAir_mentioned == 1
"""

# df_percentage_airlines = pd.read_sql_query(query_percentage_airlines, conn)
# df_percentage_airlines = df_percentage_airlines.set_axis(["Total_airways_mentioned", "British_Airways_mentioned", "AmericanAir_mentioned"], axis=0)
# df_percentage_airlines["percentage"] = 100*df_percentage_airlines["times mentioned"]/(df_percentage_airlines["times mentioned"].iloc[0])

# print(df_percentage_airlines)

query_p = f"""
SELECT user_screen_name, user_id_str AS "user", COUNT(user_id_str) AS amount
FROM tweets
GROUP BY user_id_str
ORDER BY amount DESC;
"""

df_p = pd.read_sql_query(query_p, conn)
print(df_p)
# df_p[df_p["amount"] > 10].to_csv("amount_of_tweets.csv")

query_retrieve_times = """
SELECT created_at_datetime AS datetime
FROM tweets
"""

# df_retrieve_times = pd.read_sql_query(query_retrieve_times, conn)
# df_retrieve_times["datetime"] = pd.to_datetime(df_retrieve_times["created_at_datetime"], format="%Y-%m-%d %H:%M:%S")
#
# print(df_retrieve_times)
# print(df_retrieve_times.iloc[0])
# print(type(df_retrieve_times["datetime"].iloc[0]))

# df_retrieve_times["datetime"] = (parser.parse((df_retrieve_times["created_at"].iloc[0]))).strftime("%Y-%m-%d %H:%M:%S")

# ----------- Number of bots per airline --------
bot_tweets_per_airline = {}
for airline in variables.BOT_TWEETS_PER_AIRLINE_AMOUNT.items():
    bot_tweets_per_airline[variables.AIRLINES_HANDLES[airline[0]].replace("@", "")] = airline[1]
bots_per_airline = pd.DataFrame(bot_tweets_per_airline.items(), columns=["airline", "amount"])
print(bots_per_airline)
plot_bots_per_airline = bots_per_airline.plot(kind="bar", x="airline", y="amount")
plot_bots_per_airline.get_legend().remove()
plot_bots_per_airline.set_ylabel("Amount of bots")
plot_bots_per_airline.set_title("Amount of bots per airline", size=16, weight="bold")
plt.tight_layout()
plt.show()

legend_lst = []
for i in range(13):
    query = f"""
    SELECT lang, count(lang) as tweets_lang
    FROM tweets
    WHERE {list(variables.AIRLINES.items())[i][0]}_mentioned== "1"
    GROUP BY lang
    HAVING tweets_lang > 100
    ORDER BY tweets_lang DESC
    """
    df = pd.read_sql_query(query, conn)
    try:
        if i == 0:
            plot = df.plot(kind="line", x="lang", y="tweets_lang")
            plot.set_title(f"Amount of Tweets per language {list(variables.AIRLINES.items())[i][0]}")
        else:
            df.plot(kind="line", x="lang", y="tweets_lang", ax=plot)
        legend_lst.append(list(variables.AIRLINES.items())[i][0])
    except:
        print(f"Skipped plot {list(variables.AIRLINES.items())[i][0]}")
plot.legend(legend_lst)
plt.tight_layout()
plt.show()
