import time
from transformers import AutoModelForSequenceClassification, AutoTokenizer
import pandas as pd
import sqlite3
from textblob import TextBlob
import torch
from transformers import AutoModel, AutoTokenizer
import nltk
nltk.download('vader_lexicon')
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import openpyxl


conn0 = sqlite3.connect("../database/tweets.db")
conn1 = sqlite3.connect("../sent_ana_test/database_sent_ana.sqlite")

query_db = """
SELECT id_str
FROM tweets
"""

df_db = pd.read_sql_query(query_db, conn0)
length_db = len(df_db.index)

q0 = """
SELECT text AS tweet, airline_sentiment
FROM Tweets
"""

df_0 = pd.read_sql_query(q0, conn1)
# print(df_0)

# --------------------------------------------vader---------------------------------------------------------------------
t = time.time()
begin_time = int(t*1000)

sid = SentimentIntensityAnalyzer()

df_0['scores'] = df_0['tweet'].apply(lambda review: sid.polarity_scores(review))
# print(df_0)

df_0['compound'] = df_0['scores'].apply(lambda score_dict: score_dict['compound'])
# print(df_0)

# df_0['comp_score'] = df_0['compound'].apply(lambda c: 'positive' if c > 0 "negative" elif c < 0 else 'neg')
def getAnalysis(score):
    if score < 0:
        return "negative"
    elif score == 0:
        return "neutral"
    else:
        return "positive"


df_0["comp_score"] = df_0["compound"].apply(getAnalysis)
# print(df_0)

t = time.time()
end_time = int(t*1000)
time_diff = (end_time-begin_time)/1000

print(f"The method Vader is {(100*len(df_0[df_0['airline_sentiment'] == df_0['comp_score']].index))/len(df_0.index):.2f}% correct")
print(f"Took {time_diff} seconds, time estimated for whole database: {(time_diff*(length_db/len(df_0.index)))/60:.2f} minutes")

# -------------------------------------------------bertweet (doesn't work)----------------------------------------------
# t = time.time()
# begin_time = int(t * 1000)
#
# # Load BERTweet tokenizer and model for sentiment analysis
# tokenizer = AutoTokenizer.from_pretrained("vinai/bertweet-base", use_fast=False)
# model = AutoModelForSequenceClassification.from_pretrained("vinai/bertweet-base", num_labels=3)
#
# # Example tweet
# tweet = df_0["tweet"].iloc[1]
# print(tweet)
#
# def sent_analysis(tweet):
#     # Tokenize the input tweet
#     tokens = tokenizer.encode_plus(tweet, add_special_tokens=True, truncation=True, padding="longest", return_tensors="pt")
#     input_ids = tokens["input_ids"]
#     attention_mask = tokens["attention_mask"]
#
#     # Perform sentiment analysis
#     with torch.no_grad():
#         outputs = model(input_ids=input_ids, attention_mask=attention_mask)
#
#     # Get the predicted sentiment label
#     predicted_label_id = torch.argmax(outputs.logits, dim=1)
#     sentiment_labels = ["negative", "neutral", "positive"]
#     predicted_sentiment = sentiment_labels[predicted_label_id.item()]
#
#     print("Predicted Sentiment:", predicted_sentiment)
#     return predicted_sentiment
#
# df_1 = df_0.copy()
# sent_analysis(tweet)
# df_1["analysis"] = df_1["tweet"].apply(sent_analysis)
# print(df_1)
#
# t = time.time()
# end_time = int(t*1000)
# time_diff = (end_time-begin_time)/1000
#
# print(f"The method bertweet is {(100*len(df_1[df_1['airline_sentiment'] == df_1['analysis']].index))/len(df_1.index):.2f}% correct")
# print(f"Took {time_diff} seconds, time estimated for whole database: {(time_diff*(length_db/len(df_1.index)))/60:.2f} minutes")
#

# ---------------------------------------------------TextBlob-----------------------------------------------------------
t = time.time()
begin_time = int(t*1000)

def sentiment_analysis(tweet):
    def getSubjectivity(text):
        return TextBlob(text).sentiment.subjectivity

    # Create a function to get the polarity
    def getPolarity(text):
        return TextBlob(text).sentiment.polarity

    # Create two new columns ‘Subjectivity’ & ‘Polarity’
    tweet["TextBlob_Subjectivity"] = tweet["tweet"].apply(getSubjectivity)
    tweet["TextBlob_Polarity"] = tweet["tweet"].apply(getPolarity)

    def getAnalysis(score):
        if score < 0:
            return "Negative"
        elif score == 0:
            return "Neutral"
        else:
            return "Positive"

    tweet["TextBlob_Analysis"] = tweet["TextBlob_Polarity"].apply(getAnalysis)
    return tweet

def getSubjectivity(text):
    return TextBlob(text).sentiment.subjectivity


df_0["TextBlob_Subjectivity"] = df_0["tweet"].apply(getSubjectivity)


def getPolarity(text):
    return TextBlob(text).sentiment.polarity


df_0["TextBlob_Polarity"] = df_0["tweet"].apply(getPolarity)


def getAnalysis(score):
    if score < 0:
        return "negative"
    elif score == 0:
        return "neutral"
    else:
        return "positive"


df_0["TextBlob_Analysis"] = df_0["TextBlob_Polarity"].apply(getAnalysis)
# print(df_0)

t = time.time()
end_time = int(t*1000)
time_diff = (end_time-begin_time)/1000

print(f"The method TextBlob is {(100*len(df_0[df_0['airline_sentiment'] == df_0['TextBlob_Analysis']].index))/len(df_0.index):.2f}% correct")
print(f"Took {time_diff} seconds, time estimated for whole database: {(time_diff*(length_db/len(df_0.index)))/60:.2f} minutes")
