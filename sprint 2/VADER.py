import time
import pandas as pd
import numpy as np
import sqlite3
import gc
import nltk
nltk.download("vader_lexicon")
from nltk.sentiment.vader import SentimentIntensityAnalyzer

# if sentiment analysis has already been put into parquet files, set to False. True otherwise
do_analysis = False

# set functions for Vader
sid = SentimentIntensityAnalyzer()


def sentiment_analysis_vader(score):
    if score < 0:
        return "negative"
    elif score == 0:
        return "neutral"
    else:
        return "positive"


conn = sqlite3.connect("../database/tweets.db")


if do_analysis:

    q_tweets = """
    SELECT id_str, text, lang
    FROM tweets
    WHERE lang == "en" OR lang == "und"
    """
    df_tweets = pd.read_sql_query(q_tweets, conn)

    print("starting analysis on English tweets")
    df_tweets["scores"] = df_tweets["text"].apply(lambda review: sid.polarity_scores(review))
    print("applied scores")
    df_tweets["compound"] = df_tweets["scores"].apply(lambda score_dict: score_dict["compound"])
    print("compound determined")
    df_tweets["sentiment"] = df_tweets["compound"].apply(sentiment_analysis_vader)
    print("analysis on English tweets done")

    df_tweets[["id_str", "sentiment", "compound", "lang", "scores"]].to_parquet("../database/tweets_ana.parquet")
    print("parquet file created")

    del df_tweets
    gc.collect()


    q_tweets_en = """
    SELECT tweets.id_str, translation AS text, lang
    FROM tweets, tweets_en
    WHERE tweets.id_str == tweets_en.id_str
    """
    df_tweets_en = pd.read_sql_query(q_tweets_en, conn)

    print("starting analysis on translated tweets")
    df_tweets_en["scores"] = df_tweets_en["text"].apply(lambda review: sid.polarity_scores(review))
    print("applied scores")
    df_tweets_en["compound"] = df_tweets_en["scores"].apply(lambda score_dict: score_dict["compound"])
    print("compound determined")
    df_tweets_en["sentiment"] = df_tweets_en["compound"].apply(sentiment_analysis_vader)
    print("analysis on translated tweets done")

    df_tweets_en[["id_str", "sentiment", "compound", "lang", "scores"]].to_parquet("../database/tweets_en_ana.parquet")
    print("parquet file created")

    del df_tweets_en
    gc.collect()


df_tweets = pd.read_parquet("../database/tweets_ana.parquet")
df_tweets_en = pd.read_parquet("../database/tweets_en_ana.parquet")

sentiment_tweets = []
df_tweets_length = len(df_tweets.index)
df_tweets_en_length = len(df_tweets_en.index)


df_sentiment_tweets = pd.concat([df_tweets, df_tweets_en]).astype(str)
sentiments_tweets = []
count = 0
number_of_tweets = len(df_sentiment_tweets.index)
t = time.time()
begin_time = int(t*1000)
for row in df_sentiment_tweets.iterrows():
    id = row[1]["id_str"]
    sentiment = row[1]["sentiment"]
    compound = row[1]["compound"]
    lang = row[1]["lang"]
    scores = eval(row[1]["scores"])
    neg_score = scores["neg"]
    neu_score = scores["neu"]
    pos_score = scores["pos"]
    sentiments_tweets.append({"id": id, "sentiment": sentiment, "compound": compound, "lang": lang, "neg_score": neg_score, "neu_score": neu_score, "pos_score": pos_score})
    count += 1
    if count % 100000 == 0:
        t = time.time()
        end_time = int(t*1000)
        time_took = (end_time-begin_time)/1000
        print(f"100.000 iterations of setting up the database took {time_took:.2f} seconds (at {(count/number_of_tweets*100):.2f}% and still {((number_of_tweets-count)/100000*time_took):.2f} seconds to go)")
        begin_time = end_time


df_sentiment_tweets = pd.DataFrame(sentiments_tweets)
df_sentiment_tweets.to_parquet("../database/sentiment_tweets.parquet")

table_name = "sentiment"
try:
    q = f"""
    DROP TABLE {table_name}
    """
    conn.execute(q)
except:
    pass


query = f"""
SELECT *
FROM {table_name}
"""

num_rows_inserted = df_sentiment_tweets.to_sql(table_name, conn, index=False)
cursor = conn.execute(query)
print('Sentiments successfully saved to tweets.db!')
