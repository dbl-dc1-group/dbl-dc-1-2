import hashlib

NEWEST_HASH = "a8e57ad15dca090f9df31e6da696b7ecbabc2811"
DRIVE_LINK = "https://drive.google.com/drive/folders/1AdLu7XbVeOoxsJJ74vYXba8hYEkZIPJD?usp=sharing"
CHANGE_LOG = "Converting BERT compounds improvements, updated topics"


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def hash_file(filename):
    """"This function returns the SHA-1 hash
   of the file passed into it"""

    # make a hash object
    h = hashlib.sha1()

    # open file for reading in binary mode
    with open(filename, 'rb') as file:
        # loop till the end of the file
        chunk = 0
        while chunk != b'':
            # read only 1024 bytes at a time
            chunk = file.read(1024)
            h.update(chunk)

    # return the hex representation of digest
    return h.hexdigest()


hash_local_database = hash_file("database/tweets.db")
if hash_local_database == NEWEST_HASH:
    print("You have the newest version of the database!")
else:
    print(f"{bcolors.WARNING}Download newest database version from {DRIVE_LINK} (your hash is: {hash_local_database}) \nChange log: {CHANGE_LOG}{bcolors.ENDC}")
