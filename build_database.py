print("This will take very long (days)! Especially creating the sentiment with BERT and translating")

input = input("Are you sure you wanna build the database? This will REMOVE the current database! (Y/n)")
if input == "Y":
    exec(open("load_data.py").read())
    exec(open("sprint 2/translator.py").read())
    exec(open("sprint 2/translation to SQL.py").read())
    exec(open("sprint 2/conversations.py").read())
    exec(open("sprint 4/emoji_on_AIRLINES.py").read())
    exec(open("sprint 4/BERT_CASE perform on AIRLINES.py").read())
    exec(open("sprint 4/converting_BERT_compound.py").read())
    exec(open("sprint 4/sentiment topics.py").read())
    exec(open("sprint 4/sentiment topics other_competitors.py").read())
    exec(open("sprint 4/add new topics.py").read())
