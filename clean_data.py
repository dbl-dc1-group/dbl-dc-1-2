from dateutil import parser
import variables

COLUMNS_TO_KEEP = ["created_at", "created_at_datetime", "id_str", "text", "truncated", "lang", "retweeted",
                   "is_bot_tweet", "is_duplicate_of_id",
                   "in_reply_to_status_id_str", "in_reply_to_user_id_str", "in_reply_to_screen_name", "json_file"]

keys_user = ["id_str", "screen_name", "created_at", "created_at_datetime", "statuses_count", "friends_count",
             "followers_count"]

ORDER_USER = ["user_id_str", "user_screen_name", "user_created_at", "user_created_at_datetime", "user_statuses_count",
              "user_friends_count", "user_followers_count"]

rename_dict = {"id_str": "user_id_str", "screen_name": "user_screen_name", "created_at": "user_created_at",
               "created_at_datetime": "user_created_at_datetime", "default_profile_image": "user_default_profile_image",
               "statuses_count": "user_statuses_count", "friends_count": "user_friends_count",
               "followers_count": "user_followers_count"}

unique_tweets = {}


def clean_data(line_to_clean: dict):
    tweet_info_dict = {}
    line_to_clean["is_bot_tweet"] = False
    if line_to_clean["truncated"]:  # If the tweet is truncated then set the value of full_text to text and mentions to top level
        line_to_clean["text"] = line_to_clean["extended_tweet"]["full_text"]
        line_to_clean["entities"]["user_mentions"] = line_to_clean["extended_tweet"]["entities"]["user_mentions"]
    if line_to_clean["text"][0:3] == "RT ":  # If the tweets begins with "RT " then it is a retweet
        line_to_clean["retweeted"] = True
    if line_to_clean["text"] in variables.BOT_TWEETS_AIRLINES_LIST:  # If the tweet text is in the list of all bot tweets set is_bot_tweet to True
        line_to_clean["is_bot_tweet"] = True
    try:
        line_to_clean["is_duplicate_of_id"] = unique_tweets[line_to_clean["text"]]  # Look if the same tweet is already clean, if so it is a duplicate
    except:  # Tweet is not a duplicate so set id_duplicate_of_id to None
        unique_tweets[line_to_clean["text"]] = line_to_clean["id_str"]
        line_to_clean["is_duplicate_of_id"] = None
    user_dict = {}
    dt_user = parser.parse(line_to_clean["user"]["created_at"])  # User creation time to datetime format
    line_to_clean["user"]["created_at_datetime"] = dt_user
    for item in line_to_clean["user"]:  # Get all useful attributes of user
        if item in keys_user:
            user_dict[f"user_{item}"] = line_to_clean["user"][item]
    dt = parser.parse(line_to_clean["created_at"])  # Creation date of tweet to datetime format
    line_to_clean["created_at_datetime"] = dt
    for item in line_to_clean.items():  # Clean the tweet data
        if item[0] in COLUMNS_TO_KEEP:
            tweet_info_dict[item[0]] = item[1]
    user_mentions_dict = {}
    user_mentions = line_to_clean["entities"]["user_mentions"]
    airlines_mentioned = {}
    for airline in variables.AIRLINES.items():
        airlines_mentioned[f"{airline[0]}_mentioned"] = 0
    if len(user_mentions) == 0:
        user_mentions_dict = {"mention_screen_name": 0, "mention_name": 0, "mention_id_str": 0}
    else:  # If user is mentioned add it to a list
        mention_screen_names = []
        mention_names = []
        mention_ids = []
        for item in user_mentions:
            mention_screen_names.append(item["screen_name"])
            mention_names.append(item["name"])
            mention_ids.append(item["id_str"])
        user_mentions_dict = {"mention_screen_name": mention_screen_names, "mention_name": mention_names,
                              "mention_id_str": mention_ids}

        for airline in variables.AIRLINES.items():  # If a airline is mentioned add a 1 to its mentioned column
            if str(airline[1]) in mention_ids:
                airlines_mentioned[f"{airline[0]}_mentioned"] = 1
    order_tweet_info_dict = {k: tweet_info_dict[k] for k in COLUMNS_TO_KEEP}  # Order the columns in the order of the list COLUMNS_TO_KEEP
    order_user_dict = {k: user_dict[k] for k in ORDER_USER}
    final_dict = order_tweet_info_dict | order_user_dict | user_mentions_dict | airlines_mentioned  # Join all dictionaries together
    return final_dict  # Return the dictionary with all cleaned data, ready to add to the database
