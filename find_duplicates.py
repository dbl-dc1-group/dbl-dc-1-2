import pandas as pd
import sqlite3
import variables

conn = sqlite3.connect("database/tweets.db")

NUMBER_DUPLICATES = 1
LENGTH_TWEET = 30
dct_airline_dups = {}
dct_airline_amount_dups = {}
total_dups = 0
for airline in variables.AIRLINES.items():
    query = f"""
    SELECT text
    FROM tweets
    WHERE {airline[0]}_mentioned == "1" AND SUBSTRING(text, 1, 3) != "RT " AND user_statuses_count > 50000 AND user_friends_count > 100 AND user_followers_count < 10
    """
    df = pd.read_sql_query(query, conn)
    dct = {}
    for row in df.values:
        try:
            dct[row[0]] += 1
        except:
            dct[row[0]] = 1

    sorted_dct = sorted(dct.items(), key=lambda x: x[1], reverse=True)
    converted_dict = dict(sorted_dct)
    list_dup = []
    amount_dups = 0
    for item in converted_dict.items():
        text = item[0].replace(variables.AIRLINES_HANDLES[airline[1]][1], "")
        if item[1] > NUMBER_DUPLICATES and len(text) > LENGTH_TWEET:
            list_dup.append(item[0])
            amount_dups += item[1]
            total_dups += item[1]
    dct_airline_dups[airline[1]] = list_dup
    dct_airline_amount_dups[airline[1]] = amount_dups

print(dct_airline_dups)
print(dct_airline_amount_dups)
print(f"Total duplicates for airlines: {total_dups}")
with open("dups.txt", "w", encoding="utf-8") as f:
    f.write(str(dct_airline_dups))
with open("dups_amount.txt", "w", encoding="utf-8") as f:
    f.write(str(dct_airline_amount_dups))
conn.close()
dups_list = []
for dups in dct_airline_dups.values():
    for dup in dups:
        dups_list.append(dup)

end = []
def find_airline(x: int, y: int):
    for zz in variables.AIRLINES:
        if int(x) == int(variables.AIRLINES[zz]):
            end.append((zz, y))
    return end

for z in dct_airline_amount_dups:
    find_airline(z, dct_airline_amount_dups[z])
print(end)
