# DBL-DC1

## Client

British Airways

## Main competitor

American Air

## Folders to add

- convs
- data (with the 35GB json files)
- parquet

## Link to database

https://drive.google.com/drive/folders/1AdLu7XbVeOoxsJJ74vYXba8hYEkZIPJD?usp=sharing

## Packages to install

- tensorflow
- emoji (version 0.6.0)
- sqlite3
- pandas
- matplotlib
- pyarrow
- nltk
- pytorch (with the following command:
  pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu117 --upgrade --force-reinstall)
- hashlib
- Microsoft Visual C++ 14.0 or greater
- bertopic
- seaborn
- scikit-learn
- transformers
- umap-learn
- win32com
- scipy
- tensorflow
- emot
- openpyxl
- numpy
- warnings
- pycparser
- python-dateutil
- pyshark
- variables
- pickle
- checks
- deep-translator
- gc
- os
- vaderSentiment
- re
- pytest
- itertools
- subprocess

## File to run to create database

build_database.py (WARNING: this will take days and will remove the database almost at the beginning. You DONT need to run it when you ALREADY have tweets.db in the database folder. If you don't have this you can download it via the Drive link)

## Function of the main files

### load_data.py

Loads all the json files and gives it to a clean function to delete unnecessary attributes

### clean_data.py

Cleans the data it gets from load_data.py and returns it to load_data.py

### find_duplicates.py

Find Tweets that have many duplicates

### sprint 2/conversations.py

Mine conversations from the tweets.db

### sprint 2/VADER.py

Get the sentiment of the Tweets with VADER

### sprint 4/converting_BERT_compound.py

Convert the output of BERT (probabilities) to a compound, that can have a value between -1 and 1 (-1 for strongly negative and 1 for strongly positive)

### sprint 4/BERT_CASE perform on data.py

Get the sentiment of the Tweets with BERT CASE

### sprint 4/emoij0.py

Translates emoijs in Tweets to text describing that emoij

### sprint 4/sentiment_conversation_evolution.py

Calculates the change of the sentiment in a conversation between an client and airline

### sprint 4/sentiment topics.py

Get the topics of the Tweets where British Airways or American Airlines is mentioned

### sprint 4/sentiment topics other_competitors.py

Get the topics of the Tweets where an airline other than British Airways and American Airlines is mentioned

### sprint 4/add new topics.py

Adds the relevant topics (marked in a Excel file) to the tweets.db database
