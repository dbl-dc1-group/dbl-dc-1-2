import pytest
import torch
import torch.nn.functional as F
import ast
import emoji
import re

def convert_values_to_probs(values):
    values = values.replace("]", "")
    values = values.replace("[", "")
    values = values.split(" ")
    list_probs1 = []
    for i in values:
        try:
            list_probs1.append(float(i))
        except:
            pass
    try:
        logics = torch.tensor(list_probs1)
        probs = F.softmax(logics, dim=0).tolist()
        probs = str(probs)
        return probs
    except Exception as e:
        return None


def test_convert_values_to_probs():
    assert ast.literal_eval(convert_values_to_probs('[0 10 0]')) == pytest.approx([0,1,0], abs=0.01)

def convert_compound_bert(text):
    # print(text)
    # text = text.replace("]", "")
    # text = text.replace("[", "")
    # text = text.split(" ")
    list_probs = eval(text)
    # for i in text:
    #     try:
    #         list_probs.append(float(i))
    #     except:
    #         pass
    max_value = max(list_probs)
    index_max = list_probs.index(max_value)
    sorted_list = []
    for i in list_probs:
        sorted_list.append(i)
    sorted_list.sort()
    second_max_value = sorted_list[1]
    second_max_index = list_probs.index(second_max_value)
    compound = -6
    if index_max == 0:  # Negative
        compound = max_value * -1
    elif index_max == 2:  # Positive
        compound = max_value
    elif index_max == 1:
        if second_max_index == 0:  # Negative neutral
            compound = (1 - max_value) * -1
        elif second_max_index == 2:  # Positive neutral
            compound = 1 - max_value
        else:  # There are 2 exact the same numbers in the list
            index0_value = sorted_list[0]
            index2_value = sorted_list[2]
            if index2_value > index0_value:  # Positive neutral
                compound = 1 - max_value
            else:  # Negative neutral
                compound = (1 - max_value) * -1
    if compound == -6:
        compound = None
    else:
        if compound > 1:
            compound = 1
        if compound < -1:
            compound = -1
    return compound

def test_convert_compound_bert():
    assert convert_compound_bert('[10,0,0]') == -1
    assert convert_compound_bert('[0,0,10]') == 1
    assert convert_compound_bert('[0,0.85,0.1]') ==pytest.approx(0.15, abs=0.05)

def text_cleaner(text):
    """
    cleans the given text
    :param text: text to clean
    """
    try:
        text = emoji.demojize(text)
    except:
        pass
    text = text.lower()

    # remove all unwanted characters/words
    text = re.sub(r"http\S+|www\S+", "", text)  # links
    text = re.sub(r"@\S+", "", text)  # @'s
    text = re.sub(r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}\b", "", text)  # mail adresses
    text = re.sub(r"rt ", "", text)  # 'RT '
    text = re.sub(r":\w+:", "", text)  # emoji's
    text = re.sub(r'[_"\-;%()|+&=*%.,!?:#$@\[\]/]', " ", text)  # all (random) characters
    text = re.sub(r" +", " ", text)  # multiple space after each other

    text = text.strip()
    return text

def test_text_cleaner():
    assert text_cleaner('rt https://www.youtube.com @BritishAirways  hello! %$#@%') == 'hello'

users_dct = {"1234": "12", "4627": "43", "3871": "12"}
sentiment_dct = {"1234": -0.75, "4627": 0, "3871": 0.2}
def get_sentiment_change(replies):
    replies = eval(replies)
    try:
        if users_dct[replies[0]] == users_dct[replies[-1]]:
            sentiment_change = sentiment_dct[replies[-1]] - sentiment_dct[replies[0]]
            return sentiment_change
        elif users_dct[replies[0]] == users_dct[replies[-2]]:
            sentiment_change = sentiment_dct[replies[-2]] - sentiment_dct[replies[0]]
            return sentiment_change
        else:
            return None
    except Exception as e:
        return None


def test_get_sentiment_change():

    assert get_sentiment_change("['1234', '4627', '3871']") == 0.95


pytest

