group 2:
pos:
- plots where clearly explained
- the ppt was clean

could we use this?
- no, we think our presentation has already this good things

neg:
- When you test the accuracy of a sentiment analysis tool like vader, you should use a larger sample size, 20 is a bit low
- for the plot 'change in the average sentiment per year', the line was not that clear because it was going up and down a lot
- sometimes the explantions were a bit technical
- the 25%- and 50% quartile in the 'sentiment distribution' were always 0, then why show it?

improve weak points:
- use a large dataset to verify accuracy of sentiment model for example from the site kaggle
- change the data scale, from days to 3 days for example
- keep in mind that the customer is not very technical so use simple words to explain things
- if data is not useful don't show it


group 12:
pos:
- nice idea for the different topics
- good amount of data per slide

could we use this points?
- no

neg:
- stacked bar charts: you can only compare the bottom part (positive), but the other possible outcomes are hard to compare
- numbers in pie chart are hard to read due to length and color choice

improve weak points:
- don't choose bar charts but a other chart type
- don't choose a pie chart but a bar chart for example


Group 20:
pos:
- thinking further than just the sentiment analysis
- speaking in a good tempo

could we use this point?
- we should defenitely think beyond our sentiment analysis and adjust our tempo in which we present

neg:
- voices hard to hear
- tables have many number, on the other hand, when a plot has only one bar: just write the number down.
- the code on 'sentiment analysis - data preperation', may be left out, it's very technical and not readable for the listeners
- color blindness? e.g. pink, blue and gray don't match really well

improve weak points:
- speak louder
- it's better to use a plot when using more than e.g. 3 lines of table information, this would make it more readable
- don't use (large amounts of) code in your presentation
- have a look at the colors you use, e.g. blue and orange are safe to use


group 13:
pos:
- nice extra information (how many customers) given with the number of conversation
- computed F1 scores for different models

could we use this point?
- we could have done a bit better research on other things and we could have used F1 scores to better compare models

neg:
- time in seconds/minutes/hours etc.?
- on 'sentiment & language' the mirroring of the plots doesn't add much to it, it is kind of distractive.
- have a look at the colors you use for plots for colorblind people

improve weak points:
- you should specify your units clearly
- just show plots as they are, don't do any fancy things with it like mirroring it