import pandas as pd
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
import win32com.client
import time
import os
import numpy as np

df = pd.read_csv("../sent_ana_test2/Twitter_Data.csv", encoding='latin-1')
df_to_test = df.copy(deep=True)
label_mapping = {
    "negative": -1,
    "neutral": 0,
    "positive": 1,
}

for i in range(len(df_to_test)):
    category = df_to_test.loc[i, "category"]
    for sentiment, value in label_mapping.items():
        if category == value:
            df_to_test.loc[i, "Label"] = sentiment

analyzer = SentimentIntensityAnalyzer()
t = time.time()
begin_time = int(t * 1000)

missing_count = 0

def analyze_sentiment(x):
    global missing_count
    if isinstance(x, str):
        return analyzer.polarity_scores(x)["compound"]
    else:
        missing_count += 1
        return np.nan

df_to_test["VADER_sentiment"] = df_to_test["clean_text"].apply(analyze_sentiment)

df_to_test["VADER_sentiment_label"] = df_to_test["VADER_sentiment"].apply(lambda x: "positive" if x >= 0.05 else "negative" if x <= -0.05 else "neutral")

df_to_test["accuracy"] = np.nan
for i in range(0, len(df_to_test)):
    if df_to_test["Label"].iloc[i] == df_to_test["VADER_sentiment_label"].iloc[i]:
        df_to_test.loc[i, "accuracy"] = 1
    else:
        df_to_test.loc[i, "accuracy"] = 0
total_accuracy = (sum(df_to_test["accuracy"]) / len(df_to_test["accuracy"])) * 100
t = time.time()
end_time = int(t * 1000)
accurate_time = ((end_time - begin_time) / 1000)

try:
    xl = win32com.client.Dispatch("Excel.Application")
    workbooks = xl.Workbooks
    workbook = workbooks("sentiment_results.csv")
    workbook.Close(False)
    xl.Quit()
except:
    print("Failed to close the Excel file.")

df_to_test.to_csv("sentiment_results.csv", index=False)

time.sleep(3)

try:
    excel = win32com.client.Dispatch("Excel.Application")
    excel.Visible = True
    workbook = excel.Workbooks.Open(os.path.abspath("sentiment_results.csv"))
except Exception as e:
    print("Failed to open the file:", str(e))

print(f"Time took: {accurate_time:.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_to_test) * len(df_to_test)):.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_to_test) * len(df_to_test)) / 60:.2f} minutes")
print(f"Time needed: {((accurate_time / len(df_to_test) * len(df_to_test)) / 60) / 60:.2f} hours")
print(f"Total accuracy of VADER model: {total_accuracy} %")
print(f"Number of missing or NaN values in 'clean_text' column: {missing_count}")
