import matplotlib.pyplot as plt
import pandas as pd
import sqlite3

query = """
SELECT length_convo, KLM_mentioned, AirFrance_mentioned, British_Airways_mentioned, AmericanAir_mentioned, Lufthansa_mentioned, AirBerlin_mentioned, AirBerlin_assist_mentioned, easyJet_mentioned, RyanAir_mentioned, SingaporeAir_mentioned, Qantas_mentioned, EtihadAirways_mentioned, VirginAtlantic_mentioned
FROM convos
"""

conn = sqlite3.connect("../database/tweets.db")

df_convos = pd.read_sql_query(query, conn)

airlines = ["KLM_mentioned", "AirFrance_mentioned", "British_Airways_mentioned", "AmericanAir_mentioned", "Lufthansa_mentioned", "AirBerlin_mentioned", "AirBerlin_assist_mentioned", "easyJet_mentioned", "RyanAir_mentioned", "SingaporeAir_mentioned", "Qantas_mentioned", "EtihadAirways_mentioned", "VirginAtlantic_mentioned"]
mean_convos = []
for airline in airlines:
    df = df_convos[df_convos[airline] == "1"]
    mean = df["length_convo"].mean()
    mean_convos.append({"airline": airline.replace("_mentioned", ""), "mean_convo_length": mean})

df_mean_convos = pd.DataFrame(mean_convos)
df_mean_convos = df_mean_convos.sort_values("mean_convo_length", ascending=False)
plot_convos = df_mean_convos.plot(kind="bar", x="airline", y="mean_convo_length")
plot_convos.set_title("Mean conversation length per airline", weight="bold", size=16)
plot_convos.set_xlabel("Airline")
plot_convos.set_ylabel("Conversation length")
plot_convos.get_legend().remove()

plt.tight_layout()
plt.show()
plot_convos.get_figure().savefig("figures/convo_length_per_airline.svg")
