import pandas as pd
import sqlite3
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

conn = sqlite3.connect("../database/tweets.db")


# ----------------------------- British Airways sentiment per month -----------------------------

query = """
SELECT sentiment.compound, tweets.created_at_datetime
FROM sentiment, tweets
WHERE tweets.id_str = sentiment.id AND British_Airways_mentioned == '1'
GROUP BY created_at_datetime
"""

df = pd.read_sql_query(query, conn)

df['compound'] = df['compound'].astype(float)  # Set the values of the compound value to float types

df['created_at_datetime'] = pd.to_datetime(df['created_at_datetime']) # Convert created_at_datetime values to datetime types

df.set_index('created_at_datetime', inplace=True)

df_monthly = df['compound'].resample('M').mean()

# Make the plot of sentiment per month
fig, ax = plt.subplots(figsize=(22, 27))
ax.plot(df_monthly.index, df_monthly.values)
ax.set_xlabel('Month', size=28)
ax.set_ylabel('Compound', size=28)
ax.set_title('Mean compound Over Time by British Airways(per month)', weight='bold', size=36)


ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b'))
plt.xticks(rotation=45, size=20)
plt.yticks(size=20)
plt.ylim([-0.05, 0.2])
plt.show()


# ----------------------------- American Air sentiment per month -----------------------------

query2 = """
SELECT sentiment.compound, tweets.created_at_datetime
FROM sentiment, tweets
WHERE AmericanAir_mentioned == '1' and tweets.id_str = sentiment.id
GROUP BY created_at_datetime
"""

df = pd.read_sql_query(query2, conn)

df['compound'] = df['compound'].astype(float)  # Set the values of the compound value to float types

df['created_at_datetime'] = pd.to_datetime(df['created_at_datetime']) # Convert created_at_datetime values to datetime types

df.set_index('created_at_datetime', inplace=True)

df_monthly = df['compound'].resample('M').mean()

# Make the plot of sentiment per month
fig, ax = plt.subplots(figsize=(22, 27))
ax.plot(df_monthly.index, df_monthly.values)
ax.set_xlabel('Month', size=28)
ax.set_ylabel('Compound', size=28)
ax.set_title('Mean compound Over Time by American Air(per month)', weight='bold', size=36)


ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b'))
plt.ylim([-0.05, 0.2])
plt.xticks(rotation=45, size=20)
plt.yticks(size=20)
plt.show()


# ----------------------------- All other airlines sentiment per month -----------------------------

query3 = f"""
SELECT sentiment.compound, tweets.created_at_datetime
FROM sentiment, tweets
WHERE tweets.id_str = sentiment.id AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
GROUP BY created_at_datetime
"""

df = pd.read_sql_query(query3, conn)

df['compound'] = df['compound'].astype(float)  # Set the values of the compound value to float types

df['created_at_datetime'] = pd.to_datetime(df['created_at_datetime'])  # Convert created_at_datetime values to datetime types

df.set_index('created_at_datetime', inplace=True)

df_monthly = df['compound'].resample('M').mean()

# Make the plot of sentiment per month
fig, ax = plt.subplots(figsize = (22, 27))
ax.plot(df_monthly.index, df_monthly.values)
ax.set_xlabel('Month', size=28)
ax.set_ylabel('Compound', size=28)
ax.set_title('Mean compound Over Time by all other airlines(per month)', weight='bold', size=36)


ax.xaxis.set_major_locator(mdates.MonthLocator())
ax.xaxis.set_major_formatter(mdates.DateFormatter('%b'))
plt.ylim([-0.05, 0.2])
plt.xticks(rotation=45, size=20)
plt.yticks(size=20)
plt.show()
