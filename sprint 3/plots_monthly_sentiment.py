import pandas as pd
import sqlite3
import matplotlib.pyplot as plt

pd.options.mode.chained_assignment = None  # Suppress "A value is trying to be set on a copy of a slice from a DataFrame." warning
# pd.options.mode.chained_assignment = 'warn'  # Dont suppress any warnings if needed

conn = sqlite3.connect("../database/tweets.db")

# Query all Tweets where an airline is mentioned
query_sentiment = """
SELECT id, created_at_datetime, compound, KLM_mentioned, AirFrance_mentioned, British_Airways_mentioned, AmericanAir_mentioned, Lufthansa_mentioned, AirBerlin_mentioned, AirBerlin_assist_mentioned, easyJet_mentioned, RyanAir_mentioned, SingaporeAir_mentioned, Qantas_mentioned, EtihadAirways_mentioned, VirginAtlantic_mentioned
FROM tweets, sentiment
WHERE sentiment.id == tweets.id_str AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR AirBerlin_assist_mentioned == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
"""

df_sentiment = pd.read_sql_query(query_sentiment, conn)


# Function to plot sentiment over time based on the input airline
def plot_sentiment(airline):
    row_name = ""
    if airline == "all":
        title = "Sentiment per month all airlines (except British and American)"
        file_name = "sentiment_all_airlines"
    elif airline == 'british':
        title = "Sentiment per month British Airways"
        row_name = "British_Airways_mentioned"
        file_name = "sentiment_british"
    elif airline == "american":
        title = "Sentiment per month American Air"
        row_name = "AmericanAir_mentioned"
        file_name = "sentiment_american"
    else:
        raise Exception(f"{airline} is not an airline; choose between 'british', 'american' or 'all'")
    if airline != "all":
        df_sentiment_airline = df_sentiment[df_sentiment[row_name] == "1"][["created_at_datetime", "compound", "id"]]
    else:
        df_sentiment_airline = df_sentiment[
            (df_sentiment["British_Airways_mentioned"] != "1") & (df_sentiment["AmericanAir_mentioned"] != "1")]

    # Set values to the right type in the dataframe
    df_sentiment_airline["created_at_datetime"] = pd.to_datetime(df_sentiment_airline["created_at_datetime"])
    df_sentiment_airline["month"] = df_sentiment_airline["created_at_datetime"].dt.month
    df_sentiment_airline["compound"] = pd.to_numeric(df_sentiment_airline["compound"])

    # Calculate monthly compound
    df_sentiment_per_month_airline = df_sentiment_airline[["month", "compound"]].groupby("month").mean()
    df_sentiment_per_month_airline = df_sentiment_per_month_airline.reset_index()

    # Make the plot
    plot_sentiment_british = df_sentiment_per_month_airline.plot(kind="line", x="month", y="compound")
    plot_sentiment_british.set_title(title, weight="bold", size=16, wrap=True)
    plot_sentiment_british.get_figure().tight_layout(pad=5)
    plot_sentiment_british.get_legend().remove()
    plot_sentiment_british.set_ylabel("Sentiment (-1 = negative; 1 = positive)")
    plot_sentiment_british.set_xlabel("Month of year")
    plot_sentiment_british.set_ylim(-0.1, 0.2)
    plot_sentiment_british.get_figure().savefig(f"figures/{file_name}.svg")
    plt.show()

# Generate a plot for British Airways, American Air and the rest of the airlines
plot_sentiment("british")
plot_sentiment("american")
plot_sentiment("all")
