import pandas as pd
import sqlite3
import matplotlib.pyplot as plt
import variables

conn = sqlite3.connect("../database/tweets.db")

# Generate a list with all the ids of the airline Twitter accounts
airline_ids = []
for airline in variables.AIRLINES.values():
    airline_ids.append(str(airline))

user_ids = ", ".join(airline_ids)

query_sentiment_lang = f"""
SELECT tweets.lang, COUNT(tweets.lang), sentiment.compound, SUM(sentiment.compound)
FROM tweets, sentiment
WHERE sentiment.id = tweets.id_str AND tweets.user_id_str IN ({user_ids})
GROUP BY tweets.lang
"""

df_sentiment = pd.read_sql_query(query_sentiment_lang, conn)

df_sentiment['mean_compound'] = df_sentiment['SUM(sentiment.compound)'] / df_sentiment['COUNT(tweets.lang)']
lang_list = df_sentiment['lang'].tolist()
compound_list = df_sentiment['mean_compound'].tolist()
compound_list = [float(compound) for compound in compound_list]  # Make float of every compound and append it to compound_list
fig, ax = plt.subplots()

# Set the x-axis labels as the names
ticks_x = range(len(lang_list))
ax.set_xticks(ticks_x)
ax.set_xticklabels(lang_list)

# Create the bar chart
ax.bar(ticks_x, compound_list)

# Add labels and title
ax.set_xlabel("languages")
ax.set_ylabel("sentiment")
ax.set_title("Mean sentiment per language")
ax.axhline(0, color='black')
plt.ylim([-1, 1])

# Display the chart
plt.show()

# Save the chart
ax.get_figure().savefig("figures/sentiment_per_language.png", transparent=True)
