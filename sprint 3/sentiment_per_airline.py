import pandas as pd
import sqlite3
import matplotlib.pyplot as plt

pd.options.mode.chained_assignment = None  # Suppress "A value is trying to be set on a copy of a slice from a DataFrame." warning
# pd.options.mode.chained_assignment = 'warn'  # Dont suppress any warnings if needed

conn = sqlite3.connect("../database/tweets.db")

query_sentiment_airlines = """
SELECT compound, KLM_mentioned, AirFrance_mentioned, British_Airways_mentioned, AmericanAir_mentioned, Lufthansa_mentioned, AirBerlin_mentioned, AirBerlin_assist_mentioned, easyJet_mentioned, RyanAir_mentioned, SingaporeAir_mentioned, Qantas_mentioned, EtihadAirways_mentioned, VirginAtlantic_mentioned
FROM sentiment, tweets
WHERE id == id_str AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR British_Airways_mentioned == '1' OR AmericanAir_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
"""
df_sentiment_airlines = pd.read_sql_query(query_sentiment_airlines, conn)

airlines = ["KLM_mentioned", "AirFrance_mentioned", "British_Airways_mentioned", "AmericanAir_mentioned", "Lufthansa_mentioned", "AirBerlin_mentioned", "AirBerlin_assist_mentioned", "easyJet_mentioned", "RyanAir_mentioned", "SingaporeAir_mentioned", "Qantas_mentioned", "EtihadAirways_mentioned", "VirginAtlantic_mentioned"]
airline_sentiment = []

# Generate a box plot for every airline of the sentiment distribution
for airline in airlines:

    # Prepare the dataframe for making the plot
    df = df_sentiment_airlines[df_sentiment_airlines[airline] == "1"]
    df["compound"] = df["compound"].astype(float)
    df = df[[airline, "compound"]]

    # Make the plot
    boxplot = df.plot(kind="box", x=airline, y="compound")
    boxplot.set_title(f"{airline.replace('_mentioned', '')} sentiment distribution", weight="bold", size=16)
    boxplot.set_xlabel(f"{airline.replace('_mentioned', '')}")
    boxplot.set_ylabel("Sentiment")
    boxplot.tick_params(bottom=False)
    boxplot.set(xticklabels=[])
    plt.tight_layout()
    boxplot.get_figure().savefig(f"figures/sentiment per airline/{airline.replace('_mentioned', '')}_sentiment.svg")
    plt.show()
    mean = df['compound'].mean()
    print(f"Mean of {airline} is {mean:2f}")
    airline_sentiment.append({"airline": airline.replace("_mentioned", ""), "mean_sentiment": round(mean, 2)})

# Plot the mean sentiment per airline
df_sentiment_per_airline = pd.DataFrame(airline_sentiment)
df_sentiment_per_airline = df_sentiment_per_airline.sort_values("mean_sentiment", ascending=False)
plot_sentiment_per_airline = df_sentiment_per_airline.plot(kind="bar", x="airline", y="mean_sentiment")
plot_sentiment_per_airline.get_legend().remove()
plot_sentiment_per_airline.set_title("Mean sentiment per airline", weight="bold", size=16)
plot_sentiment_per_airline.set_ylabel("Sentiment")
plot_sentiment_per_airline.set_xlabel("Airline")

plt.tight_layout()  # Make sure every text in the plot doesn't fall off the image
plot_sentiment_per_airline.get_figure().savefig("figures/sentiment_per_airline.png", transparent=True)
plt.show()
