import pandas as pd
import time
import subprocess
import win32com.client
import os
import torch
import numpy as np
from transformers import AutoModelForSequenceClassification, AutoTokenizer

df = pd.read_csv("../sent_ana_test2/Twitter_Data.csv", encoding='latin-1')

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model_name = "../sprint 3/models/distilbert_sentiment_classifier"
tokenizer_name = "../sprint 3/models/distilbert_sentiment_tokenizer"

tokenizer = AutoTokenizer.from_pretrained(tokenizer_name)
model = AutoModelForSequenceClassification.from_pretrained(model_name, from_tf=True)

model = model.to(device)

label_mapping = {
    "negative": -1,
    "neutral": 0,
    "positive": 1,
    "unknown": 2
}

label_mapping2 = {0: "negative", 1: "neutral", 2: "positive"}
df_to_test = df.copy(deep=True)
df_to_test = df_to_test.dropna(subset=["category"])
useful_labels = [-1., 0., 1.]
df_to_test = df_to_test[df_to_test["category"].isin(useful_labels)]
df_to_test["category"] = df_to_test["category"].astype(int)
df_to_test["Label"] = np.nan
df_to_test.reset_index(drop=True, inplace=True)

for i in range(len(df_to_test)):
    category = df_to_test.loc[i, "category"]
    for sentiment, value in label_mapping.items():
        if category == value:
            df_to_test.loc[i, "Label"] = sentiment

batch_size = 70
t = time.time()
begin_time = int(t * 1000)
progress = 0
percentage_to_print = 0.1

total_batches = len(df_to_test) // batch_size
for i in range(0, len(df_to_test), batch_size):
    progress_useful = (progress / total_batches * 100)
    if progress_useful >= percentage_to_print:
        print(f"Progress till the finish line in percentage: {progress_useful:.2f} %.")
        percentage_to_print += 0.1
    progress += 1
    batch_df = df_to_test.iloc[i:i + batch_size]

    input_texts = batch_df["clean_text"].astype(str).tolist()
    input_tokens = tokenizer.batch_encode_plus(
        input_texts,
        padding=True,
        truncation=True,
        return_tensors="pt"
    )
    input_ids = input_tokens["input_ids"].to(device)

    with torch.no_grad():
        predictions = model(input_ids)[0]

    predicted_labels = torch.argmax(predictions, axis=1).cpu().numpy()
    predicted_sentiments = [label_mapping2[label] for label in predicted_labels]
    df_to_test.loc[i:i + batch_size - 1, "Label_analysis"] = predicted_sentiments


df_to_test["accuracy"] = np.nan

for i in range(0, len(df_to_test)):
    if df_to_test["Label"].iloc[i] == df_to_test["Label_analysis"].iloc[i]:
        df_to_test.loc[i, "accuracy"] = 1
    else:
        df_to_test.loc[i, "accuracy"] = 0

total_accuracy = (sum(df_to_test["accuracy"])/len(df_to_test["accuracy"]))*100
t = time.time()
end_time = int(t * 1000)
accurate_time = ((end_time - begin_time) / 1000)


try:
    xl = win32com.client.Dispatch("Excel.Application")
    workbooks = xl.Workbooks
    workbook = workbooks("sentiment_results.csv")
    workbook.Close(False)
    xl.Quit()
except:
    print("Failed to close the Excel file.")

df_to_test.to_csv("sentiment_results.csv", index=False)

time.sleep(3)

try:
    excel = win32com.client.Dispatch("Excel.Application")
    excel.Visible = True
    workbook = excel.Workbooks.Open(os.path.abspath("sentiment_results.csv"))
except Exception as e:
    print("Failed to open the file:", str(e))

print(f"Time took: {accurate_time:.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_to_test) * len(df_to_test)):.2f} seconds")
print(f"Time needed: {(accurate_time / len(df_to_test) * len(df_to_test)) / 60:.2f} minutes")
print(f"Time needed: {((accurate_time / len(df_to_test) * len(df_to_test)) / 60) / 60:.2f} hours")
print(f"Total accuracy of distilBERT model: {total_accuracy} %")
