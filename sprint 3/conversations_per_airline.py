import pandas as pd
import sqlite3
import matplotlib.pyplot as plt

import functions

plt.rcParams["figure.dpi"] = 300

conn = sqlite3.connect("../database/tweets.db")
month_dct = {"04": "April (undefined Year)", "05": "May 2019", "06": "June 2019", "07": "July 2019", "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020", "": "May 2019 - March 2020"}

MONTH = ""  # Set the month for the plot, set to a empty string to get all data
if MONTH == "":
    query_convos_per_airline = """
    SELECT KLM_mentioned, AirFrance_mentioned, British_Airways_mentioned, AmericanAir_mentioned, Lufthansa_mentioned, AirBerlin_mentioned, AirBerlin_assist_mentioned, easyJet_mentioned, RyanAir_mentioned, SingaporeAir_mentioned, Qantas_mentioned, EtihadAirways_mentioned, VirginAtlantic_mentioned
    FROM convos
    """
else:
    query_convos_per_airline = f"""
    SELECT KLM_mentioned, AirFrance_mentioned, British_Airways_mentioned, AmericanAir_mentioned, Lufthansa_mentioned, AirBerlin_mentioned, AirBerlin_assist_mentioned, easyJet_mentioned, RyanAir_mentioned, SingaporeAir_mentioned, Qantas_mentioned, EtihadAirways_mentioned, VirginAtlantic_mentioned
    FROM convos
    WHERE top_tweet IN (SELECT id_str FROM tweets WHERE STRFTIME("%m", created_at_datetime) == '{MONTH}')
    """

df_convo_per_airline = pd.read_sql_query(query_convos_per_airline, conn)
airlines = ["KLM_mentioned", "AirFrance_mentioned", "British_Airways_mentioned", "AmericanAir_mentioned", "Lufthansa_mentioned", "easyJet_mentioned", "RyanAir_mentioned", "SingaporeAir_mentioned", "Qantas_mentioned", "EtihadAirways_mentioned", "VirginAtlantic_mentioned"]
df_convo_per_airline = df_convo_per_airline.astype(int)
convos_airlines = []
for airline in airlines:
    sum_airline = df_convo_per_airline[airline].sum()
    convos_airlines.append({"Airline": airline.replace("_mentioned", ""), "Number of conversations": sum_airline})
    print(f"{airline} has {sum_airline} conversations")

df_convos_airline = pd.DataFrame(convos_airlines)
df_convos_airline = df_convos_airline.sort_values("Number of conversations", ascending=False)
plot_convos_per_airline = df_convos_airline.plot(kind="bar", x="Airline", y="Number of conversations", figsize=(12,8))
plot_convos_per_airline.set_title(f"Conversations per airline in {month_dct[MONTH]}", weight="bold", size=24)
plot_convos_per_airline.tick_params(labelsize=20)
plot_convos_per_airline.set_xlabel("Airline", size=20)
plot_convos_per_airline.set_ylabel("Mean sentiment change", size=20)

plot_convos_per_airline.get_legend().remove()
plt.tight_layout(pad=3)
plt.xticks(rotation=45)
plot_convos_per_airline.get_figure().savefig("figures/convos_per_airline.png", transparent=True)
functions.open_image("figures/convos_per_airline.png")
try:
    plt.show()
except:
    print("Plot failed to show for weird reason. But it is still saved!")
