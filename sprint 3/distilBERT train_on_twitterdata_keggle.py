import pandas as pd
import time
import sqlite3
from transformers import TFDistilBertForSequenceClassification, DistilBertTokenizer
import tensorflow as tf
import numpy as np
import subprocess
import win32com.client
import os
from sklearn.model_selection import train_test_split

tf.config.list_physical_devices('GPU')

tokenizer = DistilBertTokenizer.from_pretrained("distilbert-base-cased")
model = TFDistilBertForSequenceClassification.from_pretrained("distilbert-base-cased", num_labels=3)

conn = sqlite3.connect("../sent_ana_test/database_sent_ana.sqlite")
query_for_SA = """
SELECT text, airline_sentiment 
FROM Tweets
"""
df_tweets_at_ba = pd.read_sql_query(query_for_SA, conn)
df_tweets_at_ba_copy = df_tweets_at_ba.copy(deep=True)

label_mapping = {
    "negative": 0,
    "neutral": 1,
    "positive": 2
}

df_tweets_at_ba_copy["label"] = df_tweets_at_ba_copy["airline_sentiment"].map(label_mapping)
train_df, val_df = train_test_split(df_tweets_at_ba_copy, test_size=0.2, random_state=42)

train_encodings = tokenizer(train_df["text"].tolist(), truncation=True, padding=True)
val_encodings = tokenizer(val_df["text"].tolist(), truncation=True, padding=True)

train_dataset = tf.data.Dataset.from_tensor_slices((
    dict(train_encodings),
    train_df["label"].tolist()
))

val_dataset = tf.data.Dataset.from_tensor_slices((
    dict(val_encodings),
    val_df["label"].tolist()
))

optimizer = tf.keras.optimizers.Adam(learning_rate=5e-5)
loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction=tf.keras.losses.Reduction.NONE)
metric = tf.keras.metrics.SparseCategoricalAccuracy("accuracy")

model.compile(optimizer=optimizer, loss=loss, metrics=[metric])
model.fit(train_dataset.shuffle(1000).batch(16), epochs=3, batch_size=16, validation_data=val_dataset.batch(16))

model.save_pretrained("models/distilbert_sentiment_classifier")
tokenizer.save_pretrained("models/distilbert_sentiment_tokenizer")
