import pandas as pd
import numpy as np
import tensorflow as tf
from transformers import DistilBertTokenizer, TFDistilBertForSequenceClassification

df = pd.read_csv("../sent_ana_test2/Twitter_Data.csv")

model = TFDistilBertForSequenceClassification.from_pretrained("../sprint 3/models/distilbert_sentiment_classifier")
tokenizer = DistilBertTokenizer.from_pretrained("../sprint 3/models/distilbert_sentiment_tokenizer")

label_mapping = {
    "negative": -1.0,
    "neutral": 0.0,
    "positive": 1.0
}

df_to_test = df.iloc[:100].copy(deep=True)
df_to_test["Label"] = df_to_test["category"].map(label_mapping)
print(df_to_test)
