import sqlite3
import pandas as pd
import warnings

import matplotlib as matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import seaborn as sns

sns.set()

conn = sqlite3.connect("../database/tweets.db")


# function to centre last row in our plots
def subplots_centered(nrows, ncols, nfigs):
    """
    Modification of matplotlib plt.subplots(),
    useful when some subplots are empty.

    It returns a grid where the plots
    in the **last** row are centered.

    Inputs
    nrows, ncols, figsize: same as plt.subplots()
    nfigs: real number of figures
    """
    assert nfigs < nrows * ncols, "No empty subplots, use normal plt.subplots() instead"
    fig, ax = plt.subplots(nrows=nrows, ncols=ncols, squeeze=False, figsize=(21, 12))
    axs = []

    m = nfigs % ncols
    m = range(1, ncols + 1)[-m]  # subdivision of columns
    gs = gridspec.GridSpec(nrows, m * ncols)

    for i in range(0, nfigs):
        row = i // ncols
        col = i % ncols

        if row == nrows - 1:  # center only last row
            off = int(m * (ncols - nfigs % ncols) / 2)
        else:
            off = 0

        ax = plt.subplot(gs[row, m * col + off: m * (col + 1) + off])
        axs.append(ax)

    return fig, axs


def kde_creator(df, name):
    """
    Creates kernel density plots from a dataframe and saves them.
    attributes:
    df: dataframe to create a plot of (with column name 'month' and 'compound' in it)
    name: name for title of plot and for the savefile.
    """
    fig, axs = subplots_centered(nrows=3, ncols=4, nfigs=11)
    fig.suptitle(f"Sentiment analysis per month ({name})", size=20, weight="bold")

    for i, ax in zip(range(12), axs):
        data = df[df["month"] == list(month_dct)[i]].copy()
        sns.kdeplot(data=data, x="compound", ax=ax)
        ax.set_title(list(month_dct.values())[i], size=13, weight="bold")
        ax.set_ylim(0, 4.5)
        ax.set_xlim(-1, 1)

    plt.subplots_adjust(hspace=0.4, wspace=0.5)
    plt.show()
    fig.get_figure().savefig(f"figures/sentiment analysis per month ({name}).svg")


# ignore irrelevant warnings
warnings.filterwarnings("ignore", category=FutureWarning)
warnings.filterwarnings("ignore", category=matplotlib.MatplotlibDeprecationWarning)

# setting iterable variables
month_dct = {"05": "May 2019", "06": "June 2019", "07": "July 2019", "08": "August 2019", "09": "September 2019",
             "10": "October 2019", "11": "November 2019", "12": "December 2019", "01": "January 2020",
             "02": "February 2020", "03": "March 2020"}
axes_lst = [[0, 0], [0, 1], [0, 2], [0, 3], [1, 0], [1, 1], [1, 2], [1, 3], [2, 0], [2, 1], [2, 2]]

# ---------------------------------------plots for British Airways------------------------------------------------------
# extract compound per month for British Airways
q_ba = """
SELECT STRFTIME("%m", created_at_datetime) AS month, compound
FROM tweets, sentiment
WHERE tweets.id_str == sentiment.id AND British_Airways_mentioned == '1'
ORDER BY "month"
"""

df_ba = pd.read_sql_query(q_ba, conn)
df_ba["compound"] = df_ba["compound"].astype(float)

# creates and saves the plot
kde_creator(df_ba, "British Airways")

# ---------------------------------------plots for main competitors------------------------------------------------
# extract compound per month for main competitor
q_aa = """
SELECT STRFTIME("%m", created_at_datetime) AS month, compound
FROM tweets, sentiment
WHERE tweets.id_str == sentiment.id AND AmericanAir_mentioned == '1'
ORDER BY "month"
"""

df_aa = pd.read_sql_query(q_aa, conn)
df_aa["compound"] = df_aa["compound"].astype(float)

# creates and saves the plot
kde_creator(df_aa, "main competitors AmericanAir")

# ---------------------------------------plots for all other competitors------------------------------------------------
# extract compound per month for all other competitors
q_others = """
SELECT STRFTIME("%m", created_at_datetime) AS month, compound
FROM tweets, sentiment
WHERE tweets.id_str == sentiment.id AND (KLM_mentioned == '1' OR AirFrance_mentioned == '1' OR Lufthansa_mentioned == '1' OR AirBerlin_mentioned == '1' OR 'AirBerlin_assist_mentioned' == '1' OR easyJet_mentioned == '1' OR RyanAir_mentioned == '1' OR SingaporeAir_mentioned == '1' OR Qantas_mentioned == '1' OR EtihadAirways_mentioned == '1' OR VirginAtlantic_mentioned == '1')
ORDER BY "month"
"""

df_others = pd.read_sql_query(q_others, conn)
df_others["compound"] = df_others["compound"].astype(float)

# creates and saves the plot
kde_creator(df_others, "all other competitors")
